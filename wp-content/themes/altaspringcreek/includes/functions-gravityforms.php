<?php
// change default <input> to <button> to allow for an icon
add_filter( 'gform_submit_button', 'asc_form_submit_button', 10, 5);
function asc_form_submit_button( $button, $form ) {
    $button = str_replace( "input", "button", $button );
    $button = str_replace( "/", "", $button );
    $button .= '<svg class="icon arrow"><use xlink:href="'. get_template_directory_uri() . '/dist/sprite.svg#arrow-right"></use></svg> '. $form['button']['text'];
    return $button;
}

// change ajax spinner 
add_filter( 'gform_ajax_spinner_url', 'asc_ajax_spinner_url' );
function asc_ajax_spinner_url( $src ) {
    return get_stylesheet_directory_uri() . '/assets/images/puff.svg';
}
 /*
if( !function_exists( 'gform_to_yard_timeslots' ) ) {
    
    // reference: https://docs.gravityforms.com/dynamically-populating-drop-down-fields/
   
    add_filter( 'gform_pre_render_3_15', 'gform_to_yard_timeslots' );
    add_filter( 'gform_pre_validation_3_15', 'gform_to_yard_timeslots' );
    add_filter( 'gform_pre_submission_filter_3_15', 'gform_to_yard_timeslots' );
    add_filter( 'gform_admin_pre_render_3_15', 'gform_to_yard_timeslots' );
    
    function gform_to_yard_timeslots( $form ) {
        
 
        foreach ( $posts as $post ) {
            if ( $field->type != 'select' ) === false ) {
                continue;
            }
            $choices = array();
            $choices[] = array( 'text' => $post->post_title, 'value' => $post->post_title );
            $field->choices = $choices;
        }
        
    }
}
*/
//
if( !function_exists( 'gform_to_yardi_api' ) ) {

    add_action( 'gform_after_submission_3', 'gform_to_yardi_api', 10, 2 );
    function gform_to_yardi_api( $entry, $form ) {
        
        $entry_id = $entry['id'];
        
        $e_tour_date = $entry['4'];
        $e_tour_time = $entry['13'];
        
        $e_first_name = $entry['11'];
        $e_last_name = $entry['12'];
        $e_email = sanitize_email( $entry['2'] );
        $e_phone = preg_replace('/[^0-9]/', '', $entry['3'] );
        
        $e_movein_date = $entry['6'];
        $e_bedrooms = $entry['10'];
        $e_floorplan = $entry['14'];
        // Wood Partner Stuff
        $marketingAPIkey = 'b6b2d83f-d9d6-4f89-937e-ddb474a740b1'; // Company-Wide Key
        $companyCode = 'C00000006857'; // Wood Partners Company Code
        // Property Specific info
        $propertyID = ''; 
        $propdertyCode = 'p1189627'; 
        
        $post_url = 'https://marketingapi.rentcafe.com/marketingapi/api/appointments/createleadwithappointment?' .
        'MarketingAPIKey=' . $marketingAPIkey .
        '&CompanyCode='. $companyCode .
        '&PropertyCode='. $propdertyCode .
        '&FirstName=' . $e_first_name .
        '&LastName=' . $e_last_name .
        '&Email=' . $e_email .
        '&Phone=' . $e_phone .
        '&ApptDate=' . $e_tour_date .
        '&ApptTime=' . $e_tour_time .
        '&Source=Website' .
        '&DesiredMoveinDate='. $e_movein_date .
        '&DesiredBedrooms='. $e_bedrooms .
        '&Notes=Floor Plan of Interest: ' . $e_floorplan;
        
        GFCommon::log_debug( 'gform_after_submission: First Name => ' . print_r( $e_first_name, true ) );
        GFCommon::log_debug( 'gform_after_submission: Last Name => ' . print_r( $e_first_name, true ) );
        GFCommon::log_debug( 'gform_after_submission: Email => ' . print_r( $e_email, true ) );
        GFCommon::log_debug( 'gform_after_submission: Phone => ' . print_r( $e_phone, true ) );
        GFCommon::log_debug( 'gform_after_submission: Tour Date => ' . print_r( $e_tour_date, true ) );
        GFCommon::log_debug( 'gform_after_submission: Tour time => ' . print_r( $e_tour_time, true ) );
        GFCommon::log_debug( 'gform_after_submission: Move-in Date => ' . print_r( $e_movein_date, true ) );
        GFCommon::log_debug( 'gform_after_submission: Bedrooms => ' . print_r( $e_bedrooms, true ) );
        GFCommon::log_debug( 'gform_after_submission: Floor Plan => ' . print_r( $e_floorplan, true ) );
        GFCommon::log_debug( 'gform_after_submission: Post URL => ' . print_r( $post_url, true ) );
        
        $request = new WP_Http();
        $response = $request->post( $post_url, array() );
        GFCommon::log_debug( 'gform_after_submission: response => ' . print_r( $response, true ) );
    }
}
