<?php
/**
 * Add Support for Custom Menus
 */

add_theme_support( 'menus' );
add_theme_support( 'title-tag' );
add_theme_support( 'post-thumbnails', array( 'post', 'page' ) );
add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list', 'gallery', 'caption' ) );
add_post_type_support( 'page', 'excerpt' );
add_image_size( 'grid-thumb', 620, 364, true );
// add_image_size( 'gal-slim', 1200, 400, true );
// add_image_size( 'gal-slide', 952, 600, true );

/**
 * Register Custom Menu Locations
 */

register_nav_menu( 'menu-header', 'Header' );
// register_nav_menu( 'menu-footer', 'Footer' );
// register_nav_menu( 'menu-hero', 'Hero');
// register_nav_menu( 'menu-header-offcanvas', 'Mobile Menu');

// Add body classes based on page name

function theme_body_classes( $classes ) {
  global $post;
  if( is_404() ) {
    $current_page = '404';
  } elseif( is_search() ) {
    $current_page = 'search-results';
  } else {
    $current_page = $post->post_name;
  }
  
  $classes[] = $current_page;
  return $classes;
}
add_filter( 'body_class','theme_body_classes' );

function init_enqueue_css() {
  //wp_enqueue_style( 'slick-css', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css', array(), null);
  //wp_enqueue_style( 'slick-theme-css', '//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css', array(), null);
  //wp_enqueue_style( 'google-fonts', '//fonts.googleapis.com/css?family=Josefin+Sans:300,400,400i,700', array(), null);
  wp_enqueue_style( 'flickity', 'https://unpkg.com/flickity@2/dist/flickity.min.css', array(), null );
  wp_enqueue_style( 'magnific-poup', get_template_directory_uri() . '/dist/magnific-popup.css', array(), null);
  wp_enqueue_style( 'theme', get_template_directory_uri() . '/dist/app.css', array(), null);
}

add_action( 'wp_enqueue_scripts', 'init_enqueue_css' );

function init_enqueue_js() {
  if ( ! is_admin() ) {
    wp_deregister_script( 'jquery' );
    wp_register_script( 'jquery', '//cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js', false, false, false );
    wp_enqueue_script( 'jquery' );
  }
  $api_key = 'AIzaSyA9ju5hO9ANZURJ3E-RChL_hx9aj3I6nFU';
  wp_enqueue_script( 'google-maps-api', 'https://maps.googleapis.com/maps/api/js?key=' . $api_key, array(), null, true );
  wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/assets/js/vendor/modernizr-custom.js', array(), '3.6.0', false );
  wp_enqueue_script( 'app', get_template_directory_uri() . '/dist/app.js', array( 'jquery' ), null, true );
  
  wp_enqueue_script( 'perq', 'https://altaspringcreek.fatwin.com/api/websites/resources/1?x=L2xMMm5mcDJnUFFXZVgxR3A2bXZOWG9LM1hROElmVkdpdVNITEpUeVlub1M5QnpLSWxSU1JrZzJuMGw5bVY1czJMVVpabkQvMFQ3c1h6czlxYWR5OHc9PQ2', null, null, true );
  wp_enqueue_script( 'ytp-clicktrack', 'https://t.rentcafe.com/ytpclicktrack.min.js', null, null, false );
  wp_enqueue_script( 'lead-attr', 'https://cdngeneral.rentcafe.com/JS/ThirdPartySupport/LeadAttributionAndDNIv1.2.min.js', null, null, false );

	// Localize template URL for usage in JS
	$data = array(
    'template_dir' => get_stylesheet_directory_uri(),
    'locations' => get_locations()
	);
	wp_localize_script( 'app', 'AppData', $data );
}
add_action( 'wp_enqueue_scripts', 'init_enqueue_js' );


// add the textus widget 
add_action( 'wp_footer', 'init_textus_widget' );
function init_textus_widget() {
  // https://textus.rentcafe.com/TextUsWidget.aspx?dnis=8337567561&rfs=FN,LN&SSCM=y
  echo '<script src="https://textus.rentcafe.com/js/TextUsWidget.js" id="myScript" DNIS="8337567561"></script>';
}

function init_ga_script() {
  $ga_analytics_id = get_field( 'google_analytics_id', 'option' );
  if( $ga_analytics_id ) { ?> 
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=<?php echo $ga_analytics_id; ?>"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', '<?php echo $ga_analytics_id; ?>');
    </script>

  <?php }
}
add_action( 'wp_head', 'init_ga_script' );


function init_gtag() { 
  $gt_manager_id = get_field( 'tag_manager_id', 'option' ); 
  if( $gt_manager_id ) { ?>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','<?php echo $gt_manager_id; ?>');</script>
    <!-- End Google Tag Manager -->
  <?php }
}
add_action( 'wp_head', 'init_gtag' );

function init_gtag_noscript() {
  $gt_manager_id = get_field( 'tag_manager_id', 'option' ); 
  if( $gt_manager_id ) { ?>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=<?php echo $gt_manager_id; ?>" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
  <?php }
}
add_action('after_body', 'init_gtag_noscript');


add_action( 'wp_head', 'rentcafe_lead_attribution_script' );
function rentcafe_lead_attribution_script() { 
  $api_key = 'QzAwMDAwMDA2ODU3IzEyMTkyMDEjYjZiMmQ4M2YtZDlkNi00Zjg5LTkzN2UtZGRiNDc0YTc0MGIx-sXvUqBAnyp8%3d';
  ?>
  <script type="text/javascript">
	/* Initialize LeadAttribution and DNI object */
	if (typeof RCTPCampaign != "undefined") {
		RCTPCampaign.PropertyAPIKey = '<?php echo $api_key; ?>';
		RCTPCampaign.init();
	}
	/* Initialize Click Tracking object */
	if (typeof ClickTrack != "undefined") {
		ClickTrack.SiteSection = 'PropertyPortal';
		ClickTrack._PageDisplayName = 'Lead Creation';
		ClickTrack.PropertyAPIKey = '<?php echo $api_key; ?>';
		ClickTrack.init();
	}
</script>
  <?php
}





// Add a user friendly Section Label for Custom Field Sections

function my_layout_title($title, $field, $layout, $i) {
	if($value = get_sub_field('layout_title')) {
		return $value;
	} else {
		foreach($layout['sub_fields'] as $sub) {
			if($sub['name'] == 'layout_title') {
				$key = $sub['key'];
				if(array_key_exists($i, $field['value']) && $value = $field['value'][$i][$key])
					return $value;
			}
		}
	}
	return $title;
}
add_filter('acf/fields/flexible_content/layout_title', 'my_layout_title', 10, 4);


function padding_top_classes() {
  $section_paddingtop = (!empty(get_sub_field('section_padding_top')) ? get_sub_field('section_padding_top') : '');
  if (!empty($section_paddingtop)):
    if ($section_paddingtop == 'none'):
      echo ' nopadding-top';
    elseif ($section_paddingtop == 'small'):
      echo ' paddingtop-small';
    elseif ($section_paddingtop == 'medium'):
      echo ' paddingtop-medium';
    elseif ($section_paddingtop == 'large'):
      echo ' paddingtop-large';
    elseif ($section_paddingtop == 'xlarge'):
      echo ' paddingtop-xlarge';
    elseif ($section_paddingtop == 'xxlarge'):
      echo ' paddingtop-xxlarge';
    else:
      echo '" style="padding-top:'.$section_paddingtop.'px';
    endif;
  endif;
  } 
  
function padding_bottom_classes( $pb ) {
  if( !empty( $pb )) {
    if( $pb == 'none' ) {
      echo 'nopadding-bottom';
    } elseif( $pb == 'small' ) {
      echo 'paddingbottom-small';
    } elseif( $pb == 'medium' ) {
      echo 'paddingbottom-medium';
    } elseif( $pb == 'large' ) {
      echo 'paddingbottom-large';
    } elseif( $pb == 'xlarge' ) {
      echo 'paddingbottom-xlarge';
    } elseif( $pb == 'xxlarge' ) {
      echo 'paddingbottom-xxlarge';
    }
  }
}


// Theme utility functions

require_once( get_template_directory() . '/includes/functions-acf.php' );
require_once( get_template_directory() . '/includes/functions-defaults.php' );
require_once( get_template_directory() . '/includes/functions-gravityforms.php' );
require_once( get_template_directory() . '/includes/functions-helpers.php' );
require_once( get_template_directory() . '/includes/functions-wordpress-seo.php' );
require_once( get_template_directory() . '/includes/functions-wordpress-seo-local.php' );
