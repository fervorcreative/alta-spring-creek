<?php
  $count = 1;
  $locations = get_sub_field('locations');
  $map = get_sub_field('google_map');
?>
<section id="section<?= get_row_index(); ?>" class="section google-map<?php padding_top_classes(); ?><?php padding_bottom_classes(); ?>">
  <div class="grid-container full">
    <div class="grid-x">
      <div class="cell small-12 medium-12 large-12 gmap">
        <?php $locations = get_locations(null, 'home'); ?>
        <div class="mapHolder">
          <div id="locations-map"></div>
          <select class="locations-map-select" name="mapchange">
            <option value="show-all">Show All</option>
            <option value="restaurants">Restaurants</option>
            <option value="entertainment">Entertainment</option>
            <option value="shopping">Shopping</option>
            <option value="fitness">Fitness</option>
          </select>

          <script>
            $('.locations-map-select').each(function () {
            
              // Cache the number of options
              var $this = $(this),
                numberOfOptions = $(this).children('option').length;
        
              // Hides the select element
              $this.addClass('s-hidden');
        
              // Wrap the select element in a div
              $this.wrap('<form class="select"></div>');
        
              // Insert a styled div to sit over the top of the hidden select element
              $this.after('<div class="styledSelect"></div>');
        
              // Cache the styled div
              var $styledSelect = $this.next('div.styledSelect');
        
              // Show the first select option in the styled div
              $styledSelect.text($this.children('option').eq(0).text());
        
              // Insert an unordered list after the styled div and also cache the list
              var $list = $('<ul />', {
                'class': 'options'
              }).insertAfter($styledSelect);
        
              // Insert a list item into the unordered list for each select option
              for (var i = 0; i < numberOfOptions; i++) {
                $('<li />', {
                  text: $this.children('option').eq(i).text(),
                  rel: $this.children('option').eq(i).val()
                }).appendTo($list);
              }
        
              // Cache the list items
              var $listItems = $list.children('li');
        
              // Show the unordered list when the styled div is clicked (also hides it if the div is clicked again)
              $styledSelect.click(function (e) {
                e.stopPropagation();
                $('div.styledSelect.active').each(function () {
                    $(this).removeClass('active').next('ul.options').hide();
                });
                $(this).toggleClass('active').next('ul.options').toggle();
              });
        
              // Updates the select element to have the value of the equivalent option
              $listItems.click(function (e) {
                e.stopPropagation();
                $styledSelect.text($(this).text()).removeClass('active');
                $this.val($(this).attr('rel'));
                $list.hide();
                $this.trigger('change');
              });
        
              // Hides the unordered list when clicking outside of it
              $(document).click(function () {
                $styledSelect.removeClass('active');
                $list.hide();
              });
            });
          </script>
        </div> <!-- .mapHolder -->

      </div> <!-- .cell -->
    </div> <!-- .grid-x -->
  </div> <!-- .grid-container -->
</section> <!-- section -->


