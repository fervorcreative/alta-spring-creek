<?php
/**
 * The template for displaying 404 pages (not found)
 */

get_header(); ?>

<section id="hero" class="section hero">
    <div class="grid-container full">
        <div class="grid-x">
            <div class="hero-box cell">
                <div class="hero-content">
                    <h1 class="hero-title"><?php _e( 'Oops! That page can&rsquo;t be found.' ); ?></h1>
                </div>
                <?php echo wp_get_attachment_image(499, 'full', "", array('class'=>'hero-image') ); ?>
            </div>
        </div>
    </div>
</section>
<section class="section error error-404 not-found">
  <div class="grid-container">
    
    <div class="grid-x">
      <div class="cell">
        <p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?'); ?></p>
      </div>
      <div class="cell">
        <?php get_search_form(); ?>
      </div>
    </div>
  </div>
</section>

<?php get_footer(); ?>
