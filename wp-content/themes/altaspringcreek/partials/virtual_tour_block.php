<?php 
$top_padding = get_sub_field( 'section_padding_top' );
$bottom_padding = get_sub_field( 'section_padding_bottom' ); ?>

<section id="virtual-tours" class="section <?php padding_top_classes(); ?> <?php padding_bottom_classes($bottom_padding); ?>">
  <div class="grid-container">
    <div class="grid-x grid-margin-x">
      <div class="cell small-12" data-aos="fade-right">
        <ul class="filter-items filter-tours no-bullets">
          <!-- <li class="tab current-tab"><a href="#all">All</a></li> -->
          <li><a href="javascript:;" data-filter="all">All</a></li>
          <?php $i=0; while( have_rows('virtual_tours') ) : the_row(); $i++;
            $tab_name = get_sub_field( 'tour_category' );
            $tab_name = str_replace(array(' ', '_', '®'), '-', $tab_name );
            $tab_name = strtolower( $tab_name );
            ?>
            <li><a href="javascript:;" data-filter=".<?php echo $tab_name; ?>"><?php the_sub_field( 'tour_category' ); ?></a></li>
          <?php endwhile; ?>
        </ul>
      </div>
      
      <div class="cell small-12">
        <div class="virtual-tour-units">
          <?php
          $j=0; 
          while( have_rows('virtual_tours') ) : the_row(); 
            $j++;
            $tab_name = get_sub_field( 'tour_category' );
            $tab_name = str_replace(array(' ', '_', '®'), '-', $tab_name );
            $tab_name = strtolower( $tab_name );
            $aos_delay = $j * 150; ?>
            <div class="video-tour mix <?php echo $tab_name; ?>">
              <?php 
              $t=0; 
              while( have_rows( 'videos' ) ) : the_row(); $t++;
                $tour_link = get_sub_field( 'tour_link' );
                $tour_title = get_sub_field( 'title');
                $tour_slug = str_replace(array(' ', '_'), '-', $tour_title );
                $tour_slug = strtolower( $tour_slug ); ?>
                <div class="video-tour-item">
                  <a href="javascript:;" data-href="#tour-<?php echo $tour_slug;  ?>" class="js-inline" title="<?php the_sub_field( 'title' ); ?>">
                    <?php 
                    $img = get_sub_field('video_thumbnail');
                    echo wp_get_attachment_image( $img['ID'], 'gal-thumb'); ?>
                  </a>
                  <h4><?php the_sub_field( 'title' ); ?></h4>
                  <div id="tour-<?php echo $tour_slug; ?>" class="tour-popup mfp-hide">
                    <div class="iframe-container">
                      <iframe src='<?php echo esc_url($tour_link); ?>' frameborder='0' allowfullscreen allow='vr'></iframe>
                    </div>
                  </div>
                </div>
              <?php endwhile; ?>
            </div>
          <?php endwhile; ?>
        </div>
      </div>
    </div>
  </div>
</section>