<?php
/**
 * Template part for displaying results in search pages
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class( 'entry' ); ?>>
	<header class="entry__header">
		<a href="<?php echo esc_url( get_permalink() ); ?>" title="<?php the_title(); ?>">
			<h2 class="entry-title"><?php the_title(); ?></h2>
		</a>
	</header>

	<div class="entry__summary">
		<?php the_excerpt(); ?>
	</div>
</article>
