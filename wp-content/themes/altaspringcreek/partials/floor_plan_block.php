<?php
$top_padding = get_sub_field( 'section_padding_top' );
$bottom_padding = get_sub_field( 'section_padding_bottom' ); ?>

<section class="section section--floor-plans <?php padding_top_classes(); ?> <?php padding_bottom_classes($bottom_padding); ?>">
<?php if( have_rows( 'floor_plans', 'option') ) : ?>
    <div class="grid-container">
        <div class="grid-x grid-padding-x ">
            <div class="cell">
                <ul class="filter-items no-bullets fp-filter">
                    <li><a href="javascript:;" data-filter="all">All</a></li>
                    <!-- <li><a href="javascript:;" data-filter=".studio">Studio</a></li>
                    <li><a href="javascript:;" data-filter=".one-bedroom">One-Bedroom</a></li>
                    <li><a href="javascript:;" data-filter=".two-bedrooms">Two-Bedrooms</a></li>
                    <li><a href="javascript:;" data-filter=".three-bedrooms">Three-Bedrooms</a></li> -->
                </ul>
            </div>
            <div class="cell">
                <div class="floor-plan-units"></div>
            </div>
        </div>
    </div>
    
<?php endif; ?>
</section>