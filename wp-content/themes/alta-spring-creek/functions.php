<?php
/**
 * Add Support for Custom Menus
 */

add_theme_support( 'menus' );
add_theme_support( 'title-tag' );
add_theme_support( 'post-thumbnails', array( 'post', 'page' ) );
add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list', 'gallery', 'caption' ) );
add_post_type_support( 'page', 'excerpt' );
add_image_size( 'image-grid', 640, 360 );

/**
 * Register Custom Menu Locations
 */

register_nav_menu( 'menu-header', 'Header' );
register_nav_menu( 'menu-footer', 'Footer' );
register_nav_menu( 'menu-hero', 'Hero');
register_nav_menu( 'menu-header-offcanvas', 'Mobile Menu');

// Add body classes based on page name

function potrero_body_classes( $classes ) {
  global $post;
  $current_page = $post->post_name;
  $classes[] = $current_page;
  return $classes;
}
add_filter( 'body_class','potrero_body_classes' );

function init_enqueue_css() {
  wp_enqueue_style( 'slick-css', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css', array(), null);
  wp_enqueue_style( 'slick-theme-css', '//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css', array(), null);
  wp_enqueue_style( 'google-fonts', '//fonts.googleapis.com/css?family=Josefin+Sans:300,400,400i,700', array(), null);
  wp_enqueue_style( 'magnific-poup', get_template_directory_uri() . '/dist/magnific-popup.css', array(), null);
  wp_enqueue_style( 'theme', get_template_directory_uri() . '/dist/app.css', array(), null);
}

add_action( 'wp_enqueue_scripts', 'init_enqueue_css' );

function init_enqueue_js() {
  if ( ! is_admin() ) {
    wp_deregister_script( 'jquery' );
    wp_register_script( 'jquery', '//cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js', false, false, false );
    wp_enqueue_script( 'jquery' );
  }
  wp_enqueue_script( 'slick-js', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js', array( 'jquery' ), null, true );
  wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/assets/js/vendor/modernizr-custom.js', array(), '3.6.0', false );
  wp_enqueue_script( 'app', get_template_directory_uri() . '/dist/app.js', array( 'jquery' ), null, true );

	// Localize template URL for usage in JS
	$data = array(
		'template_dir' => get_stylesheet_directory_uri(),
	);

	// Get locations
	if ( is_front_page() ) {
        global $post;

        $data['locations'] = get_locations();
    }
	wp_localize_script( 'app', 'AppData', $data );
}
add_action( 'wp_enqueue_scripts', 'init_enqueue_js' );

// Init Google Maps API

function init_gmap() { ?>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA9ju5hO9ANZURJ3E-RChL_hx9aj3I6nFU"></script>
	<?php
}
add_action('wp_head', 'init_gmap');

// Init Gmap for Advanced Custom Fields functionality

// Setup Google Map Javascript API Key for use with Advanced Custom Fields
function potrero_gmap_field_init() {
	acf_update_setting('google_api_key', 'AIzaSyA9ju5hO9ANZURJ3E-RChL_hx9aj3I6nFU');
}
add_action('acf/init', 'potrero_gmap_field_init');

function init_gtag() { ?>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-KLKPZZZ');
</script>
<?php }
//add_action( 'wp_head', 'init_gtag' );

function init_gtag_noscript() { ?>
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KLKPZZZ" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<?php
}
//add_action('after_body', 'init_gtag_noscript');

// Add a user friendly Section Label for Custom Field Sections

function my_layout_title($title, $field, $layout, $i) {
	if($value = get_sub_field('layout_title')) {
		return $value;
	} else {
		foreach($layout['sub_fields'] as $sub) {
			if($sub['name'] == 'layout_title') {
				$key = $sub['key'];
				if(array_key_exists($i, $field['value']) && $value = $field['value'][$i][$key])
					return $value;
			}
		}
	}
	return $title;
}
add_filter('acf/fields/flexible_content/layout_title', 'my_layout_title', 10, 4);


// Add a ACF Options page

if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 	=> 'Alta Spring Creek',
		'menu_title'	=> 'Theme Options',
		'menu_slug' 	=> 'alta-spring-creek',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
}

function padding_top_classes() {
  $section_paddingtop = (!empty(get_sub_field('section_padding_top')) ? get_sub_field('section_padding_top') : '');
  if (!empty($section_paddingtop)):
    if ($section_paddingtop == 'none'):
      echo ' nopadding-top';
    elseif ($section_paddingtop == 'small'):
      echo ' paddingtop-small';
    elseif ($section_paddingtop == 'medium'):
      echo ' paddingtop-medium';
    elseif ($section_paddingtop == 'large'):
      echo ' paddingtop-large';
    elseif ($section_paddingtop == 'xlarge'):
      echo ' paddingtop-xlarge';
    elseif ($section_paddingtop == 'xxlarge'):
      echo ' paddingtop-xxlarge';
    else:
      echo '" style="padding-top:'.$section_paddingtop.'px';
    endif;
  endif;
  } 
  
  function padding_bottom_classes() {
  $section_paddingbottom = (!empty(get_sub_field('section_padding_bottom')) ? get_sub_field('section_padding_bottom') : ''); 
  if (!empty($section_paddingbottom)):
    if ($section_paddingbottom == 'none'):
      echo ' nopadding-bottom';
    elseif ($section_paddingbottom == 'small'):
      echo ' paddingbottom-small';
    elseif ($section_paddingbottom == 'medium'):
      echo ' paddingbottom-medium';
    elseif ($section_paddingbottom == 'large'):
      echo ' paddingbottom-large';
    elseif ($section_paddingbottom == 'xlarge'):
      echo ' paddingbottom-xlarge';
    elseif ($section_paddingbottom == 'xxlarge'):
      echo ' paddingbottom-xxlarge';
    else:
      echo '" style="padding-bottom:'.$section_paddingbottom.'px';
    endif;
  endif;
}

// Theme utility functions

require_once( get_template_directory() . '/includes/functions-defaults.php' );
require_once( get_template_directory() . '/includes/functions-helpers.php' );
require_once( get_template_directory() . '/includes/functions-wordpress-seo.php' );
require_once( get_template_directory() . '/includes/functions-wordpress-seo-local.php' );
