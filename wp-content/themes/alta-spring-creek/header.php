<!DOCTYPE html>

<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no" />
    <link rel="stylesheet" href="https://use.typekit.net/tfk8nep.css">
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <?php wp_head(); ?>
  </head>

  <body <?php if ( get_body_class() ) body_class(); ?>>

    <?php do_action('after_body'); ?>
    
    <div id="promo-bar">
      <div id="promo">
        <h3><?php the_field('promo_bar_large_headline','option'); ?></h3>
        <h4><?php the_field('promo_bar_small_headline','option'); ?></h4>
        <div class="promo__close">
          <div class="promo-link js-promo-toggle">
            <div class="promo-link__outer">
              <div class="promo-link__icon"></div>
            </div> <!-- .promo-link__outer -->
          </div> <!-- .promo-link -->
        </div> <!-- .promo__close -->
      </div> <!-- #promo -->
    </div> <!-- #promo-bar -->

    <?php
      // Dynamically add sprite.svg if it was created
      $sprite_path = get_template_directory() . '/dist/sprite.svg';

      if ( file_exists( $sprite_path ) ) { ?>
        <script type="text/javascript">
          //Grab SVG Sprite and AJAX in so it can be cached
          var ajax = new XMLHttpRequest();
          ajax.open("GET", "<?php bloginfo('template_directory'); ?>/dist/sprite.svg", true);
          ajax.onload = function(e) {
            var svg = $(ajax.responseText);
            document.body.insertBefore(svg.get(0), document.body.childNodes[0]);
          }
          ajax.send();
        </script>
      <?php }
    ?>

    <div class="site">
      <a href="#main" class="screen-reader-text">Skip to main content</a>
      <header class="header" role="banner">
        
        <div class="grid-container full">
          <div class="grid-x grid-padding-x align-center align-middle">

            <div class="cell small-4 medium-4 large-5 header-left">
              
            </div>

            <div class="cell small-4 medium-4 large-2 header-logo">
              <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
                <img src="/wp-content/themes/alta-spring-creek/assets/images/logo.svg" alt="Alta Spring Creek">
              </a>
            </div>

            <div class="small-4 medium-4 large-5 header-right">
              <?php wp_nav_menu(array('theme_location' => 'menu-header')); ?>
            </div>

          </div>
        </div>

        <div class="nav-link js-nav-toggle">
          <div class="nav-link__outer">
            <div class="nav-link__icon"></div>
          </div> <!-- .nav-link -->
        </div> <!-- .nav-link -->
      </header>

      <main id="main" role="main">
