<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '+LgywkxEkix88GyCgNrtaKx5up9Z5ECQCntXEhJgYJxWeZqyI3yjvWlidXGtiguPB+Wit5T8fpwvvEmLoNQbqA==');
define('SECURE_AUTH_KEY',  'C7+SutmY7QrfBGu4/eAy9PqG/auug154FipW42sUHVxUmmDZGzFyKi+J0vx3aSNIYpQXTRfyQsGc7Q1Kr4xZHg==');
define('LOGGED_IN_KEY',    'R9xP0u8JFdgam4j3mWhTxRwvyF08vp7ttcaF+97CyMx7O+qs5O3i5kT9K/oAsz190ubNqsvxjs5GQhInWWFkUQ==');
define('NONCE_KEY',        'qbiFLVdfFFWF/kVGodCLFRCp1wzTwvwJf14z0gLaQMmWNYaarKCjBOXlPPjfSr6PMD4LJDDV0a0BFRqlMmMSgA==');
define('AUTH_SALT',        'Trc4j3aPEUHUB35zoX6FPVhQoHkHBmOXJZJP0tAvRA0BACaPe5FgDcwbhR/X+MT19xEoj0dRn9VKUdsPItnz7w==');
define('SECURE_AUTH_SALT', 'ZKUGsdj9DpqwTpYGRSxFPNAOrcCDxJU5e1QvgeRy/D2OM4Gl8rSeU4vtTLlhZEqDrACm8YtAqLwL1PJHjBu3bQ==');
define('LOGGED_IN_SALT',   '3ahvJ9g4kYPhdq0Yws83mxuaCOcgOLIEnDpa4IpW/PGg/8bdZBQKXT/vc07E3VZHAD/+sgmQ/rxk2/x2qdAigA==');
define('NONCE_SALT',       '+AlL/DtBkuLB4yxz/FlTQUxq8r4P4WJuHUOhTOo8fE3lFlcNu2eywUJNeKqfXkHKVabVOgVDIxzFRfujhSq4dQ==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_ba67ispgpq_';




/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
