<?php
/**
 * The template for displaying Search Results pages
 */

get_header(); ?>

<section id="hero" class="section hero">
    <div class="grid-container full">
        <div class="grid-x">
            <div class="hero-box cell">
                <div class="hero-content">
                    <h1 class="hero-title"><?php printf( __( 'Search Results for: %s' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
                </div>
                <?php echo wp_get_attachment_image(499, 'full', "", array('class'=>'hero-image') ); ?>
            </div>
        </div>
    </div>
</section>

  <?php if ( have_posts() ) : ?>

    <section class="section search-results">
      <div class="grid-container">

        

        <div class="grid-x">

          <?php while ( have_posts() ) : the_post(); ?>
            <div class="cell">
            <?php get_template_part( 'template-parts/content', 'search' ); ?>
            </div>
    

          <?php endwhile; ?>

          <?php the_posts_navigation(); ?>
          
        </div>
      </div>
    </section>
    <section class="section">
      <div class="grid-container">
        <div class="grid-x">
          <div class="cell" >
            <h5>Not what you were searching for? Try again.</h3>
            <?php get_search_form(); ?>
            </div>
        </div>
      </div>
    </section>

  <?php else : ?>

    <?php get_template_part( 'partials/content', 'none' ); ?>

  <?php endif; ?>

<?php get_footer(); ?>
