<?php
$iframe_src = get_sub_field('iframe_source');
$top_padding = get_sub_field( 'section_padding_top' );
$bottom_padding = get_sub_field( 'section_padding_bottom' );
?>
<section class="section has-bg-pattern <?php padding_top_classes(); ?> <?php padding_bottom_classes($bottom_padding); ?>">
  <div class="grid-container">
    <div class="grid-x grid-margin-x">
      <div class="cell small-12">
        <div class="iframe-block-container">
          <iframe src="<?php echo $iframe_src; ?>" frameborder="0" allowfullscreen scrolling="no"></iframe>
        </div>
      </div>
    </div>
  </div>
</section>