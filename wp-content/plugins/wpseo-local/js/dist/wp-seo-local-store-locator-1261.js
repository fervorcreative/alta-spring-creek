(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});
/**
 * wpseoLocalGeocodingRepository class for geocoding addresses.
 */
class GeocodingRepository {
	/**
  * Geocode the address based using the Google maps JavaScript geocoding API
  *
  * @var object An object containing either { "address": <address as a string> } or { "location": <the LatLng coordinates>}
  */
	static async geoCodeAddress(location) {
		const geocoder = new google.maps.Geocoder();

		if (typeof location === "object") {
			return new Promise((resolve, reject) => {
				geocoder.geocode(location, (results, status) => {
					if (status === "OK") {
						return resolve(results);
					}

					return reject(status);
				});
			});
		}

		throw new Error("Location should be an object");
	}
}
exports.default = GeocodingRepository;

},{}],2:[function(require,module,exports){
"use strict";

var _wpSeoLocalGeocodingRepository = require("./wp-seo-local-geocoding-repository.js");

var _wpSeoLocalGeocodingRepository2 = _interopRequireDefault(_wpSeoLocalGeocodingRepository);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 *
 */
class StoreLocator {
	/**
  * Constructor for the StoreLocator JS class.
  * Here we assign fields to class constants and bind methods.
  */
	constructor() {
		this.searchForm = document.querySelector("#wpseo-storelocator-form");
		this.searchInput = document.querySelector("#wpseo-sl-search");
		this.locationDetectionButton = document.querySelector(".wpseo_use_current_location");
		this.locationDetectionButtonImg = document.querySelector(".wpseo_use_current_location img");
		this.latField = document.querySelector("#wpseo-sl-lat");
		this.lngField = document.querySelector("#wpseo-sl-lng");

		this.latLng = "";

		this.locationDetection = this.locationDetection.bind(this);
		this.getLatLng = this.getLatLng.bind(this);
		this.maybeGeoCodeAddress = this.maybeGeoCodeAddress.bind(this);
		this.handleSubmitForm = this.handleSubmitForm.bind(this);
	}

	/**
  * Add event listeners to fire a function upon specified events.
  */
	addEventListeners() {
		document.addEventListener("click", this.locationDetection);
		document.addEventListener("change", this.maybeGeoCodeAddress);
		document.addEventListener("submit", this.handleSubmitForm);
	}

	/**
  * Auto detect location based on browser information.
  *
  * @param e The event passed by the event listener.
  */
	async locationDetection(e) {
		// Check both the button and the image in it for the click event.
		if (e.target === this.locationDetectionButton || e.target === this.locationDetectionButtonImg) {
			const targetInputId = this.locationDetectionButton.dataset.target;
			const targetInputField = document.querySelector("#" + targetInputId);

			// First try to get the lat and lng from the browser.
			try {
				this.latLng = await this.getLatLng();

				this.latField.value = this.latLng.lat;
				this.lngField.value = this.latLng.lng;
			} catch (error) {
				console.log(error);
			}

			// Continue the geocoding if the requested lat/lng from the browser did not result in an error.
			if (false === this.latLng instanceof Error) {
				try {
					const address = await _wpSeoLocalGeocodingRepository2.default.geoCodeAddress({ "location": this.latLng });
					targetInputField.value = address[0].formatted_address;
				} catch (error) {
					console.log(error);
				}
			}
		}
	}

	/**
  * Get the Lat and Lng position from the browser.
  *
  * @returns {Promise<*>}
  */
	async getLatLng() {
		return new Promise((resolve, reject) => {
			navigator.geolocation.getCurrentPosition(position => {
				return resolve({
					lat: parseFloat(position.coords.latitude),
					lng: parseFloat(position.coords.longitude)
				});
			}, function (error) {
				return reject(new Error("Location detection unsuccesfull"));
			});
		});
	}

	/**
  * Determine if an address should be geocoded. If so: do so.
  *
  * @param e The event passed by the event listener.
  *
  * @returns {Promise<void>}
  */
	async maybeGeoCodeAddress(e) {
		if (e.target === this.searchInput) {
			try {
				const results = await _wpSeoLocalGeocodingRepository2.default.geoCodeAddress({ address: this.searchInput.value });

				this.latField.value = results[0].geometry.location.lat();
				this.lngField.value = results[0].geometry.location.lng();
			} catch (error) {}
		}
	}

	/**
  * Catch the submit event and check wheter possibly the lat/lng data has to be calculated.
  *
  * @param e The event passed by the event listener.
  *
  * @returns {Promise<void>}
  */
	async handleSubmitForm(e) {
		if (e.target === this.searchForm) {
			e.preventDefault();

			if (this.latField.value === "" || this.lngField.value === "") {
				document.removeEventListener("submit", this.handleSubmitForm);
				const promise = new Promise(async (resolve, reject) => {
					const results = await _wpSeoLocalGeocodingRepository2.default.geoCodeAddress({ address: this.searchInput.value });

					this.latField.value = results[0].geometry.location.lat();
					this.lngField.value = results[0].geometry.location.lng();

					if (this.latField.value !== "" && this.lngField.value !== "") {
						resolve('success');
					}
				});

				const result = await promise;

				if (result === 'success') {
					this.searchForm.submit();
				}
			} else {
				this.searchForm.submit();
			}
		}
	}
}

const storeLocatorInstance = new StoreLocator();

storeLocatorInstance.addEventListeners();

},{"./wp-seo-local-geocoding-repository.js":1}]},{},[2])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJqcy9zcmMvd3Atc2VvLWxvY2FsLWdlb2NvZGluZy1yZXBvc2l0b3J5LmpzIiwianMvc3JjL3dwLXNlby1sb2NhbC1zdG9yZS1sb2NhdG9yLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7QUNBQTs7O0FBR2UsTUFBTSxtQkFBTixDQUEwQjtBQUN4Qzs7Ozs7QUFLQSxjQUFhLGNBQWIsQ0FBNkIsUUFBN0IsRUFBd0M7QUFDdkMsUUFBTSxXQUFXLElBQUksT0FBTyxJQUFQLENBQVksUUFBaEIsRUFBakI7O0FBRUEsTUFBSyxPQUFPLFFBQVAsS0FBb0IsUUFBekIsRUFBb0M7QUFDbkMsVUFBTyxJQUFJLE9BQUosQ0FBYSxDQUFFLE9BQUYsRUFBVyxNQUFYLEtBQXVCO0FBQzFDLGFBQVMsT0FBVCxDQUFrQixRQUFsQixFQUE0QixDQUFFLE9BQUYsRUFBVyxNQUFYLEtBQXVCO0FBQ2xELFNBQUssV0FBVyxJQUFoQixFQUF1QjtBQUN0QixhQUFPLFFBQVMsT0FBVCxDQUFQO0FBQ0E7O0FBRUQsWUFBTyxPQUFRLE1BQVIsQ0FBUDtBQUNBLEtBTkQ7QUFPQSxJQVJNLENBQVA7QUFTQTs7QUFFRCxRQUFNLElBQUksS0FBSixDQUFXLDhCQUFYLENBQU47QUFDQTtBQXRCdUM7a0JBQXBCLG1COzs7OztBQ0hyQjs7Ozs7O0FBRUE7OztBQUdBLE1BQU0sWUFBTixDQUFtQjtBQUNsQjs7OztBQUlBLGVBQWM7QUFDYixPQUFLLFVBQUwsR0FBa0IsU0FBUyxhQUFULENBQXdCLDBCQUF4QixDQUFsQjtBQUNBLE9BQUssV0FBTCxHQUFtQixTQUFTLGFBQVQsQ0FBd0Isa0JBQXhCLENBQW5CO0FBQ0EsT0FBSyx1QkFBTCxHQUErQixTQUFTLGFBQVQsQ0FBd0IsNkJBQXhCLENBQS9CO0FBQ0EsT0FBSywwQkFBTCxHQUFrQyxTQUFTLGFBQVQsQ0FBd0IsaUNBQXhCLENBQWxDO0FBQ0EsT0FBSyxRQUFMLEdBQWdCLFNBQVMsYUFBVCxDQUF3QixlQUF4QixDQUFoQjtBQUNBLE9BQUssUUFBTCxHQUFnQixTQUFTLGFBQVQsQ0FBd0IsZUFBeEIsQ0FBaEI7O0FBRUEsT0FBSyxNQUFMLEdBQWMsRUFBZDs7QUFFQSxPQUFLLGlCQUFMLEdBQXlCLEtBQUssaUJBQUwsQ0FBdUIsSUFBdkIsQ0FBNkIsSUFBN0IsQ0FBekI7QUFDQSxPQUFLLFNBQUwsR0FBaUIsS0FBSyxTQUFMLENBQWUsSUFBZixDQUFxQixJQUFyQixDQUFqQjtBQUNBLE9BQUssbUJBQUwsR0FBMkIsS0FBSyxtQkFBTCxDQUF5QixJQUF6QixDQUErQixJQUEvQixDQUEzQjtBQUNBLE9BQUssZ0JBQUwsR0FBd0IsS0FBSyxnQkFBTCxDQUFzQixJQUF0QixDQUE0QixJQUE1QixDQUF4QjtBQUNBOztBQUVEOzs7QUFHQSxxQkFBb0I7QUFDbkIsV0FBUyxnQkFBVCxDQUEyQixPQUEzQixFQUFvQyxLQUFLLGlCQUF6QztBQUNBLFdBQVMsZ0JBQVQsQ0FBMkIsUUFBM0IsRUFBcUMsS0FBSyxtQkFBMUM7QUFDQSxXQUFTLGdCQUFULENBQTJCLFFBQTNCLEVBQXFDLEtBQUssZ0JBQTFDO0FBQ0E7O0FBRUQ7Ozs7O0FBS0EsT0FBTSxpQkFBTixDQUF5QixDQUF6QixFQUE2QjtBQUM1QjtBQUNBLE1BQUssRUFBRSxNQUFGLEtBQWEsS0FBSyx1QkFBbEIsSUFBNkMsRUFBRSxNQUFGLEtBQWEsS0FBSywwQkFBcEUsRUFBaUc7QUFDaEcsU0FBTSxnQkFBZ0IsS0FBSyx1QkFBTCxDQUE2QixPQUE3QixDQUFxQyxNQUEzRDtBQUNBLFNBQU0sbUJBQW1CLFNBQVMsYUFBVCxDQUF3QixNQUFNLGFBQTlCLENBQXpCOztBQUVBO0FBQ0EsT0FBSTtBQUNILFNBQUssTUFBTCxHQUFjLE1BQU0sS0FBSyxTQUFMLEVBQXBCOztBQUVBLFNBQUssUUFBTCxDQUFjLEtBQWQsR0FBc0IsS0FBSyxNQUFMLENBQVksR0FBbEM7QUFDQSxTQUFLLFFBQUwsQ0FBYyxLQUFkLEdBQXNCLEtBQUssTUFBTCxDQUFZLEdBQWxDO0FBQ0EsSUFMRCxDQUtFLE9BQVEsS0FBUixFQUFnQjtBQUNqQixZQUFRLEdBQVIsQ0FBYSxLQUFiO0FBQ0E7O0FBRUQ7QUFDQSxPQUFLLFVBQVUsS0FBSyxNQUFMLFlBQXVCLEtBQXRDLEVBQThDO0FBQzdDLFFBQUk7QUFDSCxXQUFNLFVBQVUsTUFBTSx3Q0FBb0IsY0FBcEIsQ0FBb0MsRUFBRSxZQUFZLEtBQUssTUFBbkIsRUFBcEMsQ0FBdEI7QUFDQSxzQkFBaUIsS0FBakIsR0FBeUIsUUFBUyxDQUFULEVBQWEsaUJBQXRDO0FBQ0EsS0FIRCxDQUdFLE9BQVEsS0FBUixFQUFnQjtBQUNqQixhQUFRLEdBQVIsQ0FBYSxLQUFiO0FBQ0E7QUFDRDtBQUNEO0FBQ0Q7O0FBRUQ7Ozs7O0FBS0EsT0FBTSxTQUFOLEdBQWtCO0FBQ2pCLFNBQU8sSUFBSSxPQUFKLENBQWEsQ0FBRSxPQUFGLEVBQVcsTUFBWCxLQUF1QjtBQUMxQyxhQUFVLFdBQVYsQ0FBc0Isa0JBQXRCLENBQTRDLFFBQUYsSUFBZ0I7QUFDekQsV0FBTyxRQUFTO0FBQ2YsVUFBSyxXQUFZLFNBQVMsTUFBVCxDQUFnQixRQUE1QixDQURVO0FBRWYsVUFBSyxXQUFZLFNBQVMsTUFBVCxDQUFnQixTQUE1QjtBQUZVLEtBQVQsQ0FBUDtBQUlBLElBTEQsRUFLRyxVQUFVLEtBQVYsRUFBa0I7QUFDcEIsV0FBTyxPQUFRLElBQUksS0FBSixDQUFXLGlDQUFYLENBQVIsQ0FBUDtBQUNBLElBUEQ7QUFRQSxHQVRNLENBQVA7QUFVQTs7QUFFRDs7Ozs7OztBQU9BLE9BQU0sbUJBQU4sQ0FBMkIsQ0FBM0IsRUFBK0I7QUFDOUIsTUFBSyxFQUFFLE1BQUYsS0FBYSxLQUFLLFdBQXZCLEVBQXFDO0FBQ3BDLE9BQUk7QUFDSCxVQUFNLFVBQVUsTUFBTSx3Q0FBb0IsY0FBcEIsQ0FBb0MsRUFBRSxTQUFTLEtBQUssV0FBTCxDQUFpQixLQUE1QixFQUFwQyxDQUF0Qjs7QUFFQSxTQUFLLFFBQUwsQ0FBYyxLQUFkLEdBQXNCLFFBQVMsQ0FBVCxFQUFhLFFBQWIsQ0FBc0IsUUFBdEIsQ0FBK0IsR0FBL0IsRUFBdEI7QUFDQSxTQUFLLFFBQUwsQ0FBYyxLQUFkLEdBQXNCLFFBQVMsQ0FBVCxFQUFhLFFBQWIsQ0FBc0IsUUFBdEIsQ0FBK0IsR0FBL0IsRUFBdEI7QUFFQSxJQU5ELENBTUUsT0FBUSxLQUFSLEVBQWdCLENBRWpCO0FBQ0Q7QUFDRDs7QUFFRDs7Ozs7OztBQU9BLE9BQU0sZ0JBQU4sQ0FBd0IsQ0FBeEIsRUFBNEI7QUFDM0IsTUFBSyxFQUFFLE1BQUYsS0FBYSxLQUFLLFVBQXZCLEVBQW9DO0FBQ25DLEtBQUUsY0FBRjs7QUFFQSxPQUFLLEtBQUssUUFBTCxDQUFjLEtBQWQsS0FBd0IsRUFBeEIsSUFBOEIsS0FBSyxRQUFMLENBQWMsS0FBZCxLQUF3QixFQUEzRCxFQUFnRTtBQUMvRCxhQUFTLG1CQUFULENBQThCLFFBQTlCLEVBQXdDLEtBQUssZ0JBQTdDO0FBQ0EsVUFBTSxVQUFVLElBQUksT0FBSixDQUFhLE9BQU8sT0FBUCxFQUFnQixNQUFoQixLQUEyQjtBQUN2RCxXQUFNLFVBQVUsTUFBTSx3Q0FBb0IsY0FBcEIsQ0FBb0MsRUFBRSxTQUFTLEtBQUssV0FBTCxDQUFpQixLQUE1QixFQUFwQyxDQUF0Qjs7QUFFQSxVQUFLLFFBQUwsQ0FBYyxLQUFkLEdBQXNCLFFBQVMsQ0FBVCxFQUFhLFFBQWIsQ0FBc0IsUUFBdEIsQ0FBK0IsR0FBL0IsRUFBdEI7QUFDQSxVQUFLLFFBQUwsQ0FBYyxLQUFkLEdBQXNCLFFBQVMsQ0FBVCxFQUFhLFFBQWIsQ0FBc0IsUUFBdEIsQ0FBK0IsR0FBL0IsRUFBdEI7O0FBRUEsU0FBSyxLQUFLLFFBQUwsQ0FBYyxLQUFkLEtBQXdCLEVBQXhCLElBQThCLEtBQUssUUFBTCxDQUFjLEtBQWQsS0FBd0IsRUFBM0QsRUFBZ0U7QUFDL0QsY0FBUyxTQUFUO0FBQ0E7QUFDRCxLQVRlLENBQWhCOztBQVdBLFVBQU0sU0FBUyxNQUFNLE9BQXJCOztBQUVBLFFBQUksV0FBVyxTQUFmLEVBQTJCO0FBQzFCLFVBQUssVUFBTCxDQUFnQixNQUFoQjtBQUNBO0FBQ0QsSUFsQkQsTUFrQk87QUFDTixTQUFLLFVBQUwsQ0FBZ0IsTUFBaEI7QUFDQTtBQUNEO0FBQ0Q7QUF2SWlCOztBQTBJbkIsTUFBTSx1QkFBdUIsSUFBSSxZQUFKLEVBQTdCOztBQUVBLHFCQUFxQixpQkFBckIiLCJmaWxlIjoiZ2VuZXJhdGVkLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbigpe2Z1bmN0aW9uIHIoZSxuLHQpe2Z1bmN0aW9uIG8oaSxmKXtpZighbltpXSl7aWYoIWVbaV0pe3ZhciBjPVwiZnVuY3Rpb25cIj09dHlwZW9mIHJlcXVpcmUmJnJlcXVpcmU7aWYoIWYmJmMpcmV0dXJuIGMoaSwhMCk7aWYodSlyZXR1cm4gdShpLCEwKTt2YXIgYT1uZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiK2krXCInXCIpO3Rocm93IGEuY29kZT1cIk1PRFVMRV9OT1RfRk9VTkRcIixhfXZhciBwPW5baV09e2V4cG9ydHM6e319O2VbaV1bMF0uY2FsbChwLmV4cG9ydHMsZnVuY3Rpb24ocil7dmFyIG49ZVtpXVsxXVtyXTtyZXR1cm4gbyhufHxyKX0scCxwLmV4cG9ydHMscixlLG4sdCl9cmV0dXJuIG5baV0uZXhwb3J0c31mb3IodmFyIHU9XCJmdW5jdGlvblwiPT10eXBlb2YgcmVxdWlyZSYmcmVxdWlyZSxpPTA7aTx0Lmxlbmd0aDtpKyspbyh0W2ldKTtyZXR1cm4gb31yZXR1cm4gcn0pKCkiLCIvKipcbiAqIHdwc2VvTG9jYWxHZW9jb2RpbmdSZXBvc2l0b3J5IGNsYXNzIGZvciBnZW9jb2RpbmcgYWRkcmVzc2VzLlxuICovXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBHZW9jb2RpbmdSZXBvc2l0b3J5IHtcblx0LyoqXG5cdCAqIEdlb2NvZGUgdGhlIGFkZHJlc3MgYmFzZWQgdXNpbmcgdGhlIEdvb2dsZSBtYXBzIEphdmFTY3JpcHQgZ2VvY29kaW5nIEFQSVxuXHQgKlxuXHQgKiBAdmFyIG9iamVjdCBBbiBvYmplY3QgY29udGFpbmluZyBlaXRoZXIgeyBcImFkZHJlc3NcIjogPGFkZHJlc3MgYXMgYSBzdHJpbmc+IH0gb3IgeyBcImxvY2F0aW9uXCI6IDx0aGUgTGF0TG5nIGNvb3JkaW5hdGVzPn1cblx0ICovXG5cdHN0YXRpYyBhc3luYyBnZW9Db2RlQWRkcmVzcyggbG9jYXRpb24gKSB7XG5cdFx0Y29uc3QgZ2VvY29kZXIgPSBuZXcgZ29vZ2xlLm1hcHMuR2VvY29kZXIoKTtcblxuXHRcdGlmICggdHlwZW9mIGxvY2F0aW9uID09PSBcIm9iamVjdFwiICkge1xuXHRcdFx0cmV0dXJuIG5ldyBQcm9taXNlKCAoIHJlc29sdmUsIHJlamVjdCApID0+IHtcblx0XHRcdFx0Z2VvY29kZXIuZ2VvY29kZSggbG9jYXRpb24sICggcmVzdWx0cywgc3RhdHVzICkgPT4ge1xuXHRcdFx0XHRcdGlmICggc3RhdHVzID09PSBcIk9LXCIgKSB7XG5cdFx0XHRcdFx0XHRyZXR1cm4gcmVzb2x2ZSggcmVzdWx0cyApO1xuXHRcdFx0XHRcdH1cblxuXHRcdFx0XHRcdHJldHVybiByZWplY3QoIHN0YXR1cyApO1xuXHRcdFx0XHR9ICk7XG5cdFx0XHR9ICk7XG5cdFx0fVxuXG5cdFx0dGhyb3cgbmV3IEVycm9yKCBcIkxvY2F0aW9uIHNob3VsZCBiZSBhbiBvYmplY3RcIiApO1xuXHR9XG59IiwiaW1wb3J0IEdlb0NvZGluZ1JlcG9zaXRvcnkgZnJvbSBcIi4vd3Atc2VvLWxvY2FsLWdlb2NvZGluZy1yZXBvc2l0b3J5LmpzXCI7XG5cbi8qKlxuICpcbiAqL1xuY2xhc3MgU3RvcmVMb2NhdG9yIHtcblx0LyoqXG5cdCAqIENvbnN0cnVjdG9yIGZvciB0aGUgU3RvcmVMb2NhdG9yIEpTIGNsYXNzLlxuXHQgKiBIZXJlIHdlIGFzc2lnbiBmaWVsZHMgdG8gY2xhc3MgY29uc3RhbnRzIGFuZCBiaW5kIG1ldGhvZHMuXG5cdCAqL1xuXHRjb25zdHJ1Y3RvcigpIHtcblx0XHR0aGlzLnNlYXJjaEZvcm0gPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCBcIiN3cHNlby1zdG9yZWxvY2F0b3ItZm9ybVwiICk7XG5cdFx0dGhpcy5zZWFyY2hJbnB1dCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoIFwiI3dwc2VvLXNsLXNlYXJjaFwiICk7XG5cdFx0dGhpcy5sb2NhdGlvbkRldGVjdGlvbkJ1dHRvbiA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoIFwiLndwc2VvX3VzZV9jdXJyZW50X2xvY2F0aW9uXCIgKTtcblx0XHR0aGlzLmxvY2F0aW9uRGV0ZWN0aW9uQnV0dG9uSW1nID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvciggXCIud3BzZW9fdXNlX2N1cnJlbnRfbG9jYXRpb24gaW1nXCIgKTtcblx0XHR0aGlzLmxhdEZpZWxkID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvciggXCIjd3BzZW8tc2wtbGF0XCIgKTtcblx0XHR0aGlzLmxuZ0ZpZWxkID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvciggXCIjd3BzZW8tc2wtbG5nXCIgKTtcblxuXHRcdHRoaXMubGF0TG5nID0gXCJcIjtcblxuXHRcdHRoaXMubG9jYXRpb25EZXRlY3Rpb24gPSB0aGlzLmxvY2F0aW9uRGV0ZWN0aW9uLmJpbmQoIHRoaXMgKTtcblx0XHR0aGlzLmdldExhdExuZyA9IHRoaXMuZ2V0TGF0TG5nLmJpbmQoIHRoaXMgKTtcblx0XHR0aGlzLm1heWJlR2VvQ29kZUFkZHJlc3MgPSB0aGlzLm1heWJlR2VvQ29kZUFkZHJlc3MuYmluZCggdGhpcyApO1xuXHRcdHRoaXMuaGFuZGxlU3VibWl0Rm9ybSA9IHRoaXMuaGFuZGxlU3VibWl0Rm9ybS5iaW5kKCB0aGlzICk7XG5cdH1cblxuXHQvKipcblx0ICogQWRkIGV2ZW50IGxpc3RlbmVycyB0byBmaXJlIGEgZnVuY3Rpb24gdXBvbiBzcGVjaWZpZWQgZXZlbnRzLlxuXHQgKi9cblx0YWRkRXZlbnRMaXN0ZW5lcnMoKSB7XG5cdFx0ZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lciggXCJjbGlja1wiLCB0aGlzLmxvY2F0aW9uRGV0ZWN0aW9uICk7XG5cdFx0ZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lciggXCJjaGFuZ2VcIiwgdGhpcy5tYXliZUdlb0NvZGVBZGRyZXNzICk7XG5cdFx0ZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lciggXCJzdWJtaXRcIiwgdGhpcy5oYW5kbGVTdWJtaXRGb3JtICk7XG5cdH1cblxuXHQvKipcblx0ICogQXV0byBkZXRlY3QgbG9jYXRpb24gYmFzZWQgb24gYnJvd3NlciBpbmZvcm1hdGlvbi5cblx0ICpcblx0ICogQHBhcmFtIGUgVGhlIGV2ZW50IHBhc3NlZCBieSB0aGUgZXZlbnQgbGlzdGVuZXIuXG5cdCAqL1xuXHRhc3luYyBsb2NhdGlvbkRldGVjdGlvbiggZSApIHtcblx0XHQvLyBDaGVjayBib3RoIHRoZSBidXR0b24gYW5kIHRoZSBpbWFnZSBpbiBpdCBmb3IgdGhlIGNsaWNrIGV2ZW50LlxuXHRcdGlmICggZS50YXJnZXQgPT09IHRoaXMubG9jYXRpb25EZXRlY3Rpb25CdXR0b24gfHwgZS50YXJnZXQgPT09IHRoaXMubG9jYXRpb25EZXRlY3Rpb25CdXR0b25JbWcgKSB7XG5cdFx0XHRjb25zdCB0YXJnZXRJbnB1dElkID0gdGhpcy5sb2NhdGlvbkRldGVjdGlvbkJ1dHRvbi5kYXRhc2V0LnRhcmdldDtcblx0XHRcdGNvbnN0IHRhcmdldElucHV0RmllbGQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCBcIiNcIiArIHRhcmdldElucHV0SWQgKTtcblxuXHRcdFx0Ly8gRmlyc3QgdHJ5IHRvIGdldCB0aGUgbGF0IGFuZCBsbmcgZnJvbSB0aGUgYnJvd3Nlci5cblx0XHRcdHRyeSB7XG5cdFx0XHRcdHRoaXMubGF0TG5nID0gYXdhaXQgdGhpcy5nZXRMYXRMbmcoKTtcblxuXHRcdFx0XHR0aGlzLmxhdEZpZWxkLnZhbHVlID0gdGhpcy5sYXRMbmcubGF0O1xuXHRcdFx0XHR0aGlzLmxuZ0ZpZWxkLnZhbHVlID0gdGhpcy5sYXRMbmcubG5nO1xuXHRcdFx0fSBjYXRjaCAoIGVycm9yICkge1xuXHRcdFx0XHRjb25zb2xlLmxvZyggZXJyb3IgKTtcblx0XHRcdH1cblxuXHRcdFx0Ly8gQ29udGludWUgdGhlIGdlb2NvZGluZyBpZiB0aGUgcmVxdWVzdGVkIGxhdC9sbmcgZnJvbSB0aGUgYnJvd3NlciBkaWQgbm90IHJlc3VsdCBpbiBhbiBlcnJvci5cblx0XHRcdGlmICggZmFsc2UgPT09IHRoaXMubGF0TG5nIGluc3RhbmNlb2YgRXJyb3IgKSB7XG5cdFx0XHRcdHRyeSB7XG5cdFx0XHRcdFx0Y29uc3QgYWRkcmVzcyA9IGF3YWl0IEdlb0NvZGluZ1JlcG9zaXRvcnkuZ2VvQ29kZUFkZHJlc3MoIHsgXCJsb2NhdGlvblwiOiB0aGlzLmxhdExuZyB9ICk7XG5cdFx0XHRcdFx0dGFyZ2V0SW5wdXRGaWVsZC52YWx1ZSA9IGFkZHJlc3NbIDAgXS5mb3JtYXR0ZWRfYWRkcmVzcztcblx0XHRcdFx0fSBjYXRjaCAoIGVycm9yICkge1xuXHRcdFx0XHRcdGNvbnNvbGUubG9nKCBlcnJvciApO1xuXHRcdFx0XHR9XG5cdFx0XHR9XG5cdFx0fVxuXHR9XG5cblx0LyoqXG5cdCAqIEdldCB0aGUgTGF0IGFuZCBMbmcgcG9zaXRpb24gZnJvbSB0aGUgYnJvd3Nlci5cblx0ICpcblx0ICogQHJldHVybnMge1Byb21pc2U8Kj59XG5cdCAqL1xuXHRhc3luYyBnZXRMYXRMbmcoKSB7XG5cdFx0cmV0dXJuIG5ldyBQcm9taXNlKCAoIHJlc29sdmUsIHJlamVjdCApID0+IHtcblx0XHRcdG5hdmlnYXRvci5nZW9sb2NhdGlvbi5nZXRDdXJyZW50UG9zaXRpb24oICggcG9zaXRpb24gKSA9PiB7XG5cdFx0XHRcdHJldHVybiByZXNvbHZlKCB7XG5cdFx0XHRcdFx0bGF0OiBwYXJzZUZsb2F0KCBwb3NpdGlvbi5jb29yZHMubGF0aXR1ZGUgKSxcblx0XHRcdFx0XHRsbmc6IHBhcnNlRmxvYXQoIHBvc2l0aW9uLmNvb3Jkcy5sb25naXR1ZGUgKSxcblx0XHRcdFx0fSApO1xuXHRcdFx0fSwgZnVuY3Rpb24oIGVycm9yICkge1xuXHRcdFx0XHRyZXR1cm4gcmVqZWN0KCBuZXcgRXJyb3IoIFwiTG9jYXRpb24gZGV0ZWN0aW9uIHVuc3VjY2VzZnVsbFwiICkgKTtcblx0XHRcdH0gKTtcblx0XHR9ICk7XG5cdH1cblxuXHQvKipcblx0ICogRGV0ZXJtaW5lIGlmIGFuIGFkZHJlc3Mgc2hvdWxkIGJlIGdlb2NvZGVkLiBJZiBzbzogZG8gc28uXG5cdCAqXG5cdCAqIEBwYXJhbSBlIFRoZSBldmVudCBwYXNzZWQgYnkgdGhlIGV2ZW50IGxpc3RlbmVyLlxuXHQgKlxuXHQgKiBAcmV0dXJucyB7UHJvbWlzZTx2b2lkPn1cblx0ICovXG5cdGFzeW5jIG1heWJlR2VvQ29kZUFkZHJlc3MoIGUgKSB7XG5cdFx0aWYgKCBlLnRhcmdldCA9PT0gdGhpcy5zZWFyY2hJbnB1dCApIHtcblx0XHRcdHRyeSB7XG5cdFx0XHRcdGNvbnN0IHJlc3VsdHMgPSBhd2FpdCBHZW9Db2RpbmdSZXBvc2l0b3J5Lmdlb0NvZGVBZGRyZXNzKCB7IGFkZHJlc3M6IHRoaXMuc2VhcmNoSW5wdXQudmFsdWUgfSApO1xuXG5cdFx0XHRcdHRoaXMubGF0RmllbGQudmFsdWUgPSByZXN1bHRzWyAwIF0uZ2VvbWV0cnkubG9jYXRpb24ubGF0KCk7XG5cdFx0XHRcdHRoaXMubG5nRmllbGQudmFsdWUgPSByZXN1bHRzWyAwIF0uZ2VvbWV0cnkubG9jYXRpb24ubG5nKCk7XG5cblx0XHRcdH0gY2F0Y2ggKCBlcnJvciApIHtcblxuXHRcdFx0fVxuXHRcdH1cblx0fVxuXG5cdC8qKlxuXHQgKiBDYXRjaCB0aGUgc3VibWl0IGV2ZW50IGFuZCBjaGVjayB3aGV0ZXIgcG9zc2libHkgdGhlIGxhdC9sbmcgZGF0YSBoYXMgdG8gYmUgY2FsY3VsYXRlZC5cblx0ICpcblx0ICogQHBhcmFtIGUgVGhlIGV2ZW50IHBhc3NlZCBieSB0aGUgZXZlbnQgbGlzdGVuZXIuXG5cdCAqXG5cdCAqIEByZXR1cm5zIHtQcm9taXNlPHZvaWQ+fVxuXHQgKi9cblx0YXN5bmMgaGFuZGxlU3VibWl0Rm9ybSggZSApIHtcblx0XHRpZiAoIGUudGFyZ2V0ID09PSB0aGlzLnNlYXJjaEZvcm0gKSB7XG5cdFx0XHRlLnByZXZlbnREZWZhdWx0KCk7XG5cblx0XHRcdGlmICggdGhpcy5sYXRGaWVsZC52YWx1ZSA9PT0gXCJcIiB8fCB0aGlzLmxuZ0ZpZWxkLnZhbHVlID09PSBcIlwiICkge1xuXHRcdFx0XHRkb2N1bWVudC5yZW1vdmVFdmVudExpc3RlbmVyKCBcInN1Ym1pdFwiLCB0aGlzLmhhbmRsZVN1Ym1pdEZvcm0gKTtcblx0XHRcdFx0Y29uc3QgcHJvbWlzZSA9IG5ldyBQcm9taXNlKCBhc3luYyAocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG5cdFx0XHRcdFx0Y29uc3QgcmVzdWx0cyA9IGF3YWl0IEdlb0NvZGluZ1JlcG9zaXRvcnkuZ2VvQ29kZUFkZHJlc3MoIHsgYWRkcmVzczogdGhpcy5zZWFyY2hJbnB1dC52YWx1ZSB9ICk7XG5cblx0XHRcdFx0XHR0aGlzLmxhdEZpZWxkLnZhbHVlID0gcmVzdWx0c1sgMCBdLmdlb21ldHJ5LmxvY2F0aW9uLmxhdCgpO1xuXHRcdFx0XHRcdHRoaXMubG5nRmllbGQudmFsdWUgPSByZXN1bHRzWyAwIF0uZ2VvbWV0cnkubG9jYXRpb24ubG5nKCk7XG5cblx0XHRcdFx0XHRpZiAoIHRoaXMubGF0RmllbGQudmFsdWUgIT09IFwiXCIgJiYgdGhpcy5sbmdGaWVsZC52YWx1ZSAhPT0gXCJcIiApIHtcblx0XHRcdFx0XHRcdHJlc29sdmUoICdzdWNjZXNzJyApO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fSk7XG5cblx0XHRcdFx0Y29uc3QgcmVzdWx0ID0gYXdhaXQgcHJvbWlzZTtcblxuXHRcdFx0XHRpZiggcmVzdWx0ID09PSAnc3VjY2VzcycgKSB7XG5cdFx0XHRcdFx0dGhpcy5zZWFyY2hGb3JtLnN1Ym1pdCgpO1xuXHRcdFx0XHR9XG5cdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHR0aGlzLnNlYXJjaEZvcm0uc3VibWl0KCk7XG5cdFx0XHR9XG5cdFx0fVxuXHR9XG59XG5cbmNvbnN0IHN0b3JlTG9jYXRvckluc3RhbmNlID0gbmV3IFN0b3JlTG9jYXRvcigpO1xuXG5zdG9yZUxvY2F0b3JJbnN0YW5jZS5hZGRFdmVudExpc3RlbmVycygpOyJdfQ==
