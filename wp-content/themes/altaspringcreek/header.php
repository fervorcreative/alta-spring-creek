<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no" />
    <meta name="referrer" content="origin">
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <link rel="stylesheet" href="https://use.typekit.net/tfk8nep.css">
    <?php wp_head(); ?>
  </head>

  <body <?php if ( get_body_class() ) body_class(); ?>>

    <?php do_action('after_body'); ?>
    <?php
    $promo = get_field( 'main_headline', 'option' );
    $p_disc = get_field( 'supporting_text', 'option' );
    ?>
    <?php if( $promo || $p_disc ) { ?>
    <div id="promo-bar">
      <div id="promo">
        <?php if( $promo ) { ?><h3 class="promo"><?php echo $promo; ?></h3><?php } ?>
        <?php if( $p_disc ) { ?><p class="disclaimer"><?php echo $p_disc; ?></p><?php } ?>
      </div> <!-- #promo -->
      <div class="promo__close">
          <div class="promo-link js-promo-toggle">
            <div class="promo-link__outer">
              <div class="promo-link__icon"></div>
            </div> <!-- .promo-link__outer -->
          </div> <!-- .promo-link -->
        </div> <!-- .promo__close -->
    </div> <!-- #promo-bar -->
    <?php } ?>
    <?php
      // Dynamically add sprite.svg if it was created
      $sprite_path = get_template_directory() . '/dist/sprite.svg';

      if ( file_exists( $sprite_path ) ) { ?>
        <script type="text/javascript">
          //Grab SVG Sprite and AJAX in so it can be cached
          var ajax = new XMLHttpRequest();
          ajax.open("GET", "<?php bloginfo('template_directory'); ?>/dist/sprite.svg", true);
          ajax.onload = function(e) {
            var svg = $(ajax.responseText);
            document.body.insertBefore(svg.get(0), document.body.childNodes[0]);
          }
          ajax.send();
        </script>
      <?php }
    ?>

    <div class="site">
      <a href="#main" class="screen-reader-text">Skip to main content</a>
      <header class="header" role="banner">
        
        <div class="grid-container">
          <div class="grid-x grid-padding-x align-center align-middle">

            <div class="cell large-auto header-left">
              
            </div>

            <div class="cell small-6  large-auto header-logo">
              <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
                <img class="logo" src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.svg" alt="Alta Spring Creek">
                <img class="logo-gold" src="<?php echo get_template_directory_uri(); ?>/assets/images/spring-creek-gold.svg" alt="<?php echo get_bloginfo('name'); ?>">
              </a>
            </div>

            <div class="cell small-6 large-auto header-right">
              <?php //wp_nav_menu(array('theme_location' => 'menu-header')); ?>
              <nav class="header-right-links">
                <a href="https://yccservices.yardi.com/i3root/webchat/webchat.php?chatID=246411" class="custom-window" target="_blank">Live Chat</a>
                <?php 
                $phone_number = get_field( 'phone_number', 'option'); 
                $call_number = str_replace( array('-', '.', '(', ')'), '', $phone_number );
                ?>
                <?php if( $phone_number ) {
                  echo '<a class="phone dniphonehref" href="tel:+1'. $call_number .'"><span class="dniphone">'. $phone_number .'</span></a>';
                } ?>
                <div class="nav-link js-nav-toggle">
                  <div class="nav-link__outer">
                    <div class="nav-link__icon"></div>
                  </div> <!-- .nav-link -->
                </div> <!-- .nav-link -->
              </nav>
              
            </div>

          </div>
        </div>

        
      </header>
      <div id="site-nav" class="">
        <div class="grid-container">
          <div class="grid-x grid-padding-x grid-padding-y align-center align-middle">
            <div class="cell small-4 medium-4 large-4"></div>
            <div class="cell small-4 medium-4 large-4 header-logo">
              <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/spring-creek-gold.svg" alt="<?php echo get_bloginfo('name'); ?>">
              </a>
            </div>
            <div class="cell small-4 medium-4 large-4 header-right">
              <div class="nav-close-link js-nav-close-toggle">
                <div class="nav-close-link__outer">
                  <div class="nav-close-link__icon"></div>
                </div> <!-- .nav-link -->
              </div> <!-- .nav-link -->
            </div>
          </div>
        </div>
        <div class="grid-container">
          <div class="grid-x grid-padding-x align-center align-middle">
            <div class="cell small-12">
              <?php wp_nav_menu(array('theme_location' => 'menu-header', 'menu_container' => 'nav')); ?>
            </div>
            <div class="cell small-12">
                <nav class="connect-links">
                <?php if( $phone_number ) {
                  echo '<a class="phone dniphonehref" href="tel:+1'. $call_number .'"><span class="dniphone">'. $phone_number .'</span></a>';
                } ?>
                  <a href="#" onclick="return changeToTextLinkIfPhone(this, DNIS, TextUSPopupRequest);">Text Us</a>
                  <a href="https://yccservices.yardi.com/i3root/webchat/webchat.php?chatID=246411" class="custom-window" target="_blank">Live Chat</a>
                  <a href="https://altaspringcreek.securecafe.com/residentservices/alta-spring-creek/userlogin.aspx" target="_blank">Resident Login</a>
                </nav>
                <?php
                $fb = get_field( 'facebook', 'option' );
                $tw = get_field( 'twitter', 'option' );
                $inst = get_field( 'instagram', 'option');
                ?>
                <?php if( $fb || $tw || $inst ) { echo '<nav class="connect-links social--links">'; } ?>
                
                  <?php if( $fb ) { echo '<a href="'. $fb .'" target="_blank">Facebook</a>'; } ?>
                  <?php if( $inst ) { echo '<a href="'. $inst .'" target="_blank">Instagram</a>'; } ?>
                  <?php if( $tw ) { echo '<a href="'. $tw .'" target="_blank">Twitter</a>'; } ?>
                  
                <?php if( $fb || $tw || $inst ) { echo '</nav>'; } ?>
            </div>
          </div>
        </div>
      </div>
      <main id="main" role="main">
