<?php 
  
  $background_color = get_sub_field('bg_color');
  $form_id = get_sub_field('form_id');
  $section_title = get_sub_field('section_title');
  $title = get_sub_field('title');
  $copy = get_sub_field('copy'); ?>

<section id="section" class="section contact-form<?php padding_top_classes(); ?><?php padding_bottom_classes(); ?>" data-aos="fade-up" data-aos-delay="300" style="<?= (!empty($background_color) ? 'background-color:'.$background_color : ''); ?>">
  <div class="grid-container">
    <div class="grid-x align-middle align-center">
      <div class="cell small-12 medium-7 large-7">
        <div class="subpage-textbox">
          <h6 class="contact-title"><?= $title; ?></h6>
          <?= $copy; ?>
        </div> <!-- .textbox -->
      </div> <!-- .col-lg-6 -->

      <div class="cell small-12 medium-5 large-5 textbox-right">
        <div class="form-holder">
          <?php echo do_shortcode('[gravityform id='.$form_id.' title=false description=false ajax=true]'); ?>
        </div> <!-- .form-holder -->
    </div> <!-- .row -->
  </div> <!-- .container-fluid -->
</section>