<?php 
  
  $background_color = get_sub_field('bg_color');
  $form_id = get_sub_field('form_id');
  $section_title = get_sub_field('section_title');
  $title = get_sub_field('title');
  $copy = get_sub_field('copy'); 
  $offset_form = get_sub_field( 'offset_form' ); 
  $bottom_padding = get_sub_field( 'section_padding_button' );
?>

<section id="contact" class="section contact-form <?php echo ($offset_form ? 'offset-form' : ''); ?> <?php padding_top_classes(); ?> <?php padding_bottom_classes($bottom_padding); ?>">
    <div class="grid-container" data-aos="fade-right">
        <div class="grid-x text-center align-center">
            <div class="cell">
              <h3><?php echo $title; ?></h3>
              <?php if( $copy ) echo $copy; ?>
              <div class="form">
                  <?php echo do_shortcode('[gravityform id='.$form_id.' title=false description=false ajax=true]'); ?>
              </div> <!-- .form -->
            </div> <!-- .cell -->
        </div> <!-- .grid-x -->
    </div> <!-- .grid-container -->
</section>