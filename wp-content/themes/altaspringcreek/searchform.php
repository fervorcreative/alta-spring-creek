<form role="search" class="search-form" action="<?php echo home_url( '/' ); ?>" method="get">
    <label class="screen-reader-text" for="s">
        <?php echo _x( 'Search for:', 'label' ) ?>
    </label>
    <span class="screen-reader-text"></span>
    <input type="text" name="s" id="search" class="input" placeholder="<?php echo esc_attr_x( 'Search …', 'placeholder' ) ?>" value="<?php echo get_search_query() ?>" />
    <input type="submit" class="button" value="<?php echo esc_attr_x( 'Search', 'submit button' ) ?>" />
</form>