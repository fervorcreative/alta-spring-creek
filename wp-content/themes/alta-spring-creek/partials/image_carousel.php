<?php 

$images = get_sub_field('gallery');

//print_r($images);

?>

<section class="section image-carousel">
  <div class="grid-container full">
    <div class="grid-x">
      <div class="cell small-12 medium-12 large-12">
        <div class="center">
          <?php 
            foreach ($images as $image):
              echo '<div><img src="'.$image["image"]["url"].'" alt="" /></div>';
            endforeach; ?>
        </div>
        <div class="slick-nav">
          <div class="prev">&xlarr;</div>
          <div class="next">&xrarr;</div>
        </div> <!-- .slick-nav -->
      </div> <!-- .cell -->
    </div> <!-- .grid-x --> 
  </div> <!-- .grid-container --> 
</section>