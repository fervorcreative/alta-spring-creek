<?php
get_header();

get_template_part( 'template-parts/content', 'hero' );

while( have_posts() ) : the_post(); ?>

    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <?php get_template_part( 'template-parts/content', 'page' ); ?>
    </article>
    
    
    
<?php endwhile; 

get_footer(); ?>