<?php

$hero_image = get_sub_field('hero_image');
$logo = get_sub_field('logo');
$logo_text_1 = get_sub_field('logo_text_1');
$logo_text_2 = get_sub_field('logo_text_2');
$logo_tagline = get_sub_field('logo_tagline');
$banner_headline = get_sub_field('banner_headline');
$custom_label = get_sub_field('custom_label'); ?>

<section class="hero bg-black">
	<div class="grid-container full">
		<div class="grid-x">
			<div class="hero__box cell" data-aos="fade">
				<div class="hero__content">
					<h2 class="hero__title display-h1"><?= $banner_headline; ?></h2>
					<!--<a class="hero__link button" href="#contact">Email me when tours begin</a>-->
				</div> <!-- .hero_content -->
				<img class="hero__image" src="<?= $hero_image['url']; ?>" alt="Luxurious common room.">
			</div> <!-- .hero__box -->
		</div> <!-- .grid-x -->
	</div> <!-- .grid-container -->
</section>