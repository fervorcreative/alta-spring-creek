<?php 
$title = get_sub_field( 'title' );
$images = get_sub_field('gallery');
$bottom_padding = get_sub_field( 'section_padding_button' );
//var_dump($images);
?>

<section class="section image-carousel <?php padding_bottom_classes($bottom_padding); ?>">
  <div class="grid-container">
    <div class="grid-x align-center">
      <?php if( ! empty( $title ) ) { ?>
      <div class="cell small-12 paddingbottom-small">
        <h4 class="serif text-center gray-text"><strong><?php echo $title; ?></strong></h4>
      </div>
      <?php } ?>
      <div class="cell small-12 large-10">
        <div class="image-slider">
          <?php foreach ($images as $image): ?>
              <div class="carousel-cell">
                <?php echo wp_get_attachment_image( $image['ID'], 'gal-slide'); ?>
              </div>
          <?php endforeach; ?>
        </div>
        <button class="flickity-button flickity-prev-next-button previous custom" type="button" aria-label="Previous">Prev</button>
        <button class="flickity-button flickity-prev-next-button next custom" type="button" aria-label="Next">Next</button>
        <div class="slider-stats text-center"></div>
      </div> <!-- .cell -->
    </div> <!-- .grid-x --> 
  </div> <!-- .grid-container --> 
</section>