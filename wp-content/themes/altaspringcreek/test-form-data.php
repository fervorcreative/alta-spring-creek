<?php
$errors = array();
$data = array();

// user info
$firstname = strip_tags( trim( $_POST['FirstName'] ) );
$lastname = strip_tags( trim( $_POST['LastName'] ) );
$email = filter_var(trim($_POST["Email"]), FILTER_SANITIZE_EMAIL);
$phone = strip_tags( trim( $_POST['Phone'] ) );

// details
$ApptDate = strip_tags( trim( $_POST['ApptDate']));
if( isset( $_POST['ApptTime'])) { $ApptTime = strip_tags( trim( $_POST['ApptTime'])); }
$DesiredMoveinDate = strip_tags( trim( $_POST['DesiredMoveinDate']));
if( isset( $_POST['DesiredBedrooms'] )) { $DesiredBedrooms = strip_tags( trim( $_POST['DesiredBedrooms']) ); }
$Notes = 'Floor Plan of Interest:' . strip_tags( trim( $_POST['FloorPlan']) );

if( isset( $_POST['Submit'] ) ) {
  echo $firstname . ' ' . $lastname;
}