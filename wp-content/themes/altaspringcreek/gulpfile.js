// Initialize modules
const { src, dest, watch, series, parallel } = require('gulp');

const plumber = require('gulp-plumber');
const rename = require('gulp-rename');
const notify = require('gulp-notify');

const eslint = require('gulp-eslint');
const uglify = require('gulp-uglify');
const concat = require('gulp-concat');

const sass = require('gulp-sass');
sass.compiler = require('node-sass');
const sassglob = require('gulp-sass-glob');

const postcss = require('gulp-postcss');
const cssnano = require('gulp-cssnano');
const autoprefixer = require('autoprefixer');
const rucksack = require('rucksack-css');
const sourcemaps = require('gulp-sourcemaps');

const imagemin = require('gulp-imagemin');
const svgstore = require('gulp-svgstore');

// Variables
const files = {
  sassPath: 'assets/sass/app.sass',
  sassFilesPath: 'assets/sass/**/*.sass',
  jsPath: 'assets/js/*.js',
  svgPath: 'assets/svg/*.svg'
}

// List any JS dependencies from node_modules first in array
// Ex: 'node_modules/scrollmagic/scrollmagic/minified/plugins/jquery.ScrollMagic.min.js'
const jsFiles = [
  'node_modules/object-fit-images/dist/ofi.min.js',
  'node_modules/aos/dist/aos.js',
  'node_modules/flickity/dist/flickity.pkgd.min.js',
  'node_modules/mixitup/dist/mixitup.min.js',
  'node_modules/svgxuse/svgxuse.min.js',
  'node_modules/air-datepicker-amd/dist/js/datepicker.min.js',
  'node_modules/air-datepicker-amd/dist/js/i18n/datepicker.en.js',
  'node_modules/jquery-validation/dist/jquery.validate.min.js',
  'node_modules/jquery-validation/dist/additional-methods.min.js',
  files.jsPath
]

// Alert
const alert = function(error) {
  notify.onError({
    title:    'Gulp',
    subtitle: 'Failure!',
    message:  'Error: <%= error.message %>',
    sound:    'Submarine',
    onLast:   true
  })(error);

  this.emit('end');
}

// Compile Sass to CSS
function sassTask() {
  return src(files.sassPath)
    .pipe(plumber({
      errorHandler: alert
    }))
    .pipe(sourcemaps.init())
    .pipe(sassglob())
    .pipe(sass())
    .pipe(postcss([
      autoprefixer(),
      rucksack()
    ]))
    .pipe(cssnano())
    .pipe(sourcemaps.write('.'))
    .pipe(dest('dist'))
    .pipe(notify({
      title:    'Gulp Task Complete',
      message:  'Sass compilation complete',
      sound:    'Submarine',
      onLast:   true
    }));
}

// Compile JavaScript
function jsTask() {
  return src(jsFiles)
    .pipe(plumber({
      errorHandler: alert
    }))
    .pipe(eslint())
    .pipe(eslint.format())
    .pipe(eslint.failAfterError())
    .pipe(concat('app.js'))
    .pipe(uglify())
    .pipe(dest('dist'))
    .pipe(notify({
      title:   'Gulp Task Complete',
      message: 'JavaScript compilation complete',
      sound:   'Submarine',
      onLast:  true
    }));
}

// Create SVG Sprite
function spriteTask() {
  return src(files.svgPath)
    .pipe(plumber({ errorHandler: alert }))
    .pipe(imagemin({
      progressive: true,
      svgoPlugins: [{removeUselessStrokeAndFill: true}]
    }))
    .pipe(svgstore({ inlineSvg: true }))
    .pipe(rename('sprite.svg'))
    .pipe(dest('dist'))
    .pipe(notify({
      title:   'Gulp Task Complete',
      message: 'SVG sprite creation complete',
      sound:   'Submarine',
      onLast:  true
    }));
}

// Watch Sass and JS Files
function watchTask() {
  watch([files.sassFilesPath], sassTask);
  watch([files.jsPath], jsTask);
}

// Compile both CSS and JS Files
const buildTask = parallel(sassTask, jsTask);

// Export Tasks
exports.build = buildTask;
exports.sass = sassTask;
exports.js = jsTask;
exports.sprite = spriteTask;
exports.watch = series(buildTask, watchTask);
exports.default = buildTask;
