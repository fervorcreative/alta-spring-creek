/* global google, AppData, objectFitImages, AOS */

// Set up App object and jQuery
var App = App || {},
  $ = $ || jQuery;

App.objectFitImg = function() {
  objectFitImages();
}

App.navLink = function() {
  $('.js-nav-toggle').on( 'click', function() {
    // Show nav overlay
    if ($('html').hasClass('is-open')) {
      $('.nav--header').fadeToggle(200);
      $('html').toggleClass('is-open');
    // Hide nav overlay
    } else {
      $('.nav--header').fadeToggle(250);
      window.setTimeout(function() {
        $('html').toggleClass('is-open');
      }, 50);
    }
  });
};

App.fadeAnimations = function() {
  AOS.init({
    disable: 'tablet',
    offset: 200,
    duration: 1000,
    once: true,
  });
}

App.smoothScroll = function() {
  $('a[href^=\\#]:not([href=\\#])').on('click', function(event) {
    var target = $.attr(this, 'href');
    var targetPosition = $(target).offset().top;
    var currentPosition = $('.site').offset().top;

    $('html, body').stop().animate({
        scrollTop: targetPosition - currentPosition
    }, 400);

    event.preventDefault();
  });
}

App.sliderInit = function() {
  $('.alta-carousel').slick({
    centerMode: true,
    arrows: false,
    dots: true,
    centerPadding: '',
    slidesToShow: 1,
    adaptiveHeight: false,
    variableWidth: false,
    //prevArrow: $('.prev'),
    //nextArrow: $('.next'),
  });
}

App.sliderFixedHeightImages = function() {
  $('.alta-carousel').on('setPosition', function () {
    AltaResizeSlider();
  });
   
  //we need to maintain a set height when a resize event occurs.
  //Some events will through a resize trigger: $(window).trigger('resize');
  $(window).on('resize', function(e) {
    AltaResizeSlider();
  });
   
  //since multiple events can trigger a slider adjustment, we will control that adjustment here
  function AltaResizeSlider(){
    $slickSlider = $('.alta-carousel');
    $slickSlider.find('.slick-slide').height('auto');
   
    var slickTrack = $slickSlider.find('.slick-track');
    var slickTrackHeight = $(slickTrack).height();
   
    $slickSlider.find('.slick-slide').css('height', slickTrackHeight + 'px');
  }
}

App.magnificLightboxImageGridInit = function() {
  $('.gallery-grid').magnificPopup({
    image: {
      markup: '<div class="mfp-figure">' +
      '<div class="mfp-toolbar"></div>' + 
      '<div class="mfp-close"></div>' +
      '<div class="product-container">'+
      '<figure>' +
      '<div class="mfp-img"></div>' +
      '<figcaption>' +
      '<div class="mfp-bottom-bar">' +
      '<div class="mfp-title"></div>' +
      '<div class="mfp-counter"></div>' +
      '</div>' +
      '</figcaption>' +
      '</figure>' +
      '</div>'+
      '</div>'
    },
    delegate: 'a',
    type: 'image',
    removalDelay: 500,
    callbacks: {
      beforeOpen: function() {
        this.st.image.markup = this.st.image.markup.replace('mfp-figure', 'mfp-figure mfp-with-anim');
        this.st.mainClass = this.st.el.attr('data-effect');
      }
    },
    midClick: true,
    gallery: {
      enabled:true
    }
  });
}

App.promoBarToggle = function() {
  var timeInHours = .25
  var expirationDuration = 1000 * 60 * 60 * timeInHours;
  var prevAccepted = localStorage.getItem('promoShown');
  var currentTime = new Date().getTime();
  var notAccepted = prevAccepted === undefined;
  var prevAcceptedExpired = prevAccepted !== undefined && currentTime - prevAccepted > expirationDuration
  if (notAccepted || prevAcceptedExpired) {
    localStorage.setItem('promoShown', currentTime);
  } else {
    $('#promo-bar').hide();
  }
} //function

App.promoClose = function() {
  $('.promo__close').on('click', function() {
    $('#promo, .promo__close').slideUp(250, function() {
      $('header').css('top', '0');
    });
    $('#logo').removeClass('hidden');
  });
  
}

App.stickyNav = function() {
  // Create a clone of the menu, right next to original
  $('.header').addClass('original').clone().insertAfter('.header').stop(true,true).addClass('cloned bg-beige', 500).css('position','fixed').css('top','0').css('margin-top','0').css('z-index','500').removeClass('original').hide();

  scrollIntervalID = setInterval(stickIt, 10);

  function stickIt() {
  
  var orgElementPos = $('.original').offset();
    orgElementTop = orgElementPos.top;               

    if ($(window).scrollTop() > (orgElementTop)  ) {
      // scrolled past the original position; now only show the cloned, sticky element.

      // Cloned element should always have same left position and width as original element.     
      orgElement = $('.original');
      coordsOrgElement = orgElement.offset();
      leftOrgElement = coordsOrgElement.left;  
      widthOrgElement = orgElement.css('width');
      $('.cloned').css('left',leftOrgElement+'px').css('top',0).css('width',widthOrgElement).show();
      $('.original').css('visibility','hidden');
    } else {
      // not scrolled past the menu; only show the original menu.
      $('.cloned').hide();
      $('.original').css('visibility','visible');
    }
  }
}

App.triggerVirtualTour = function() {
  $('.virtual-tour-link a').addClass('fancybox-iframe');
}

App.locationsMap = function() {
  var map_trigger = $('[data-location-id]'),
    map_options = {
      zoom : 15,
      mapTypeId : google.maps.MapTypeId.ROADMAP,
      disableDefaultUI: false,
      mapTypeControl: false,
      draggable: true,
      styles: [{
        'featureType': 'administrative',
        'elementType': 'labels.text.fill',
        'stylers': [{
        'color': '#444444'
        }]
      },{
        'featureType': 'landscape',
        'elementType': 'all',
        'stylers': [{
        'color': '#f2f2f2'
        }]
      },{
        'featureType': 'poi',
        'elementType': 'all',
        'stylers': [{
          'visibility': 'on'
        },{
          //'color': '#000000'
        },{
          'saturation': -100
        }]
      },{
        'featureType': 'road',
        'elementType': 'all',
        'stylers': [{
            'saturation': -100
        },{
            'color': '#d5d5d5'
        },{
            'lightness': 45
        }]
      },{
        'featureType': 'road.highway',
        'elementType': 'all',
        'stylers': [{
        'visibility': 'simplified',
        'color': '#000000'
        }]
      },{
        'featureType': 'road.highway',
        'elementType': 'geometry.fill',
        'stylers': [{
        'color': '#d5d5d5'
        }]
      },{
        'featureType': 'road.arterial',
        'elementType': 'labels.icon',
        'stylers': [{
        'visibility': 'off'
        }]
      },{
        'featureType': 'transit',
        'elementType': 'all',
        'stylers': [{
        'visibility': 'off'
        }]
      },{
        'featureType': 'water',
        'elementType': 'all',
        'stylers': [{
            'color': '#dde6e8'
            },{
            'visibility': 'on'
        }]
      }]
    },
    map = new google.maps.Map(document.getElementById('locations-map'), map_options),
    infowindow = new google.maps.InfoWindow(),
    markers = [],
    infowindows = [];
  
  // Go to and center the selected location
  map_trigger.on('click', function(e) {
    e.preventDefault();
    var location_id = parseInt($(this).attr('data-location-id'));
    infowindow.close();
    infowindow.setContent(infowindows[location_id]);
    infowindow.open(map, markers[location_id]);
    map.panTo(markers[location_id].getPosition());
    markers[location_id].setZIndex(google.maps.Marker.MAX_ZINDEX + 1);
  });

  // filter_dropdown.on('change', function(e) {
  //   var category = $(this).val();
  //   filterLocations( category );
  // });

  // Filter the map markers
  function filterLocations(category) {
    var i;

    if ( !category ) {
      category = 'show-all';
    }

    // Go through and hide/show markers while calculating the bounds
    for ( i = 0; i < markers.length; i++ ) {
      
      if ( markers[i] ) {
        //alert(markers[i].category);
        if (markers[i].category === category && markers[i] !== 'home') {
          show(category);  
        } else if (markers[i].category != category && category != 'show-all' && markers[i].category !== 'home') {
          hide(category);
        } else if (category == 'show-all') {
          showAllMarkers();
        }

        function show(category) {
          //infowindow.close( map, marker[i] );
          markers[i].setVisible(true);
          //alert('show');
        }

        function hide(category) {
          //infowindow.close( map, marker[i] );
          markers[i].setVisible(false);
        }

        function showAllMarkers() {
          //infowindow.close( map, marker[i] );
          markers[i].setVisible();
        }
      }
    }
  }
  
  // Create bounds to help center map
  var bounds = new google.maps.LatLngBounds();
  var locations = AppData.locations;

  for (var i = 0; i < locations.length; i += 1) {
    (function() {
      var location = locations[i],
        latitude = location.custom_fields._wpseo_coordinates_lat,
        longitude = location.custom_fields._wpseo_coordinates_long,
        latLng = new google.maps.LatLng(latitude, longitude),
        marker = new google.maps.Marker({
          map: map,
          title: location.post_title,
          position: latLng,
          zIndex: 1,
          category: location.category.slug,
        }),
        markerSlug = '',
        content = [
          '<div class="infowindow">',
          '<h4>' + location.post_title + '</h4>',
          '<p class="address">',
          location.custom_fields._wpseo_business_address + ',',
          location.custom_fields._wpseo_business_city + ', ',
          location.custom_fields._wpseo_business_state + ' ',
          location.custom_fields._wpseo_business_zipcode,
          '</p>',
          '</div>',
        ].join('');
      // Determine what marker icon
      if ( location.category.slug === 'home' ) {
        markerSlug = 'marker-'+location.category.slug;
      } else {
        markerSlug = 'marker';
      }
      var icon = { url: AppData.template_dir + '/assets/images/map-markers/'+markerSlug + '.png' }
      // Extend the bounds to include each marker's position
      bounds.extend(latLng);
      //console.log('locations', locations);
      //console.log('markers', markers);
      markers[location.ID] = marker;
      infowindows[location.ID] = content;
      marker.setIcon(icon);

      google.maps.event.addListener(marker, 'click', function() {
        infowindow.close();
        infowindow.setContent(content);
        infowindow.open(map, this);
        this.setZIndex(google.maps.Marker.MAX_ZINDEX + 1);
      });
      google.maps.event.addDomListener(window, 'resize', function() {
        var mapCenter = map.getCenter();
        google.maps.event.trigger(map, 'resize');
        
        //map.setCenter(36.0352249, -115.039589);
      });
    })();
  }
  // Center map based on bounds
  map.setCenter(new google.maps.LatLng(32.970467, -96.656225));
}

App.animatePlaceholders = function() {
  $('input').focus(function(){
    placeholder = $(this).attr('placeholder');
    if(placeholder != undefined){
      $(this).parent().prepend('<span class="input-placeholder">'+placeholder+'</span>');
      $(this).attr('placeholder', '');
    }
  });

  $('input').blur(function(){
    $(this).attr('placeholder', $('.input-placeholder').text());
    $(this).parent().find('.input-placeholder').remove();
  });
}

App.animateHomeBoxes = function() {
  $('.box-image').on('touchstart', function() {
    //alert('Touchstart!');
    //Used this function to add touchstart event, which allows Safari to operate with CSS animations
  })
}

App.stickyNav = function() {
  // Create a clone of the menu, right next to original
  $('.header').addClass('original').clone().insertAfter('.header').addClass('cloned').css('position','fixed').css('top','0').css('margin-top','0').css('z-index','500').removeClass('original').hide();

  scrollIntervalID = setInterval(stickIt, 10);

  function stickIt() {
  
  var orgElementPos = $('.original').offset();
    orgElementTop = orgElementPos.top;               

    if ($(window).scrollTop() >= (orgElementTop)  ) {
      orgElement = $('.original');
      coordsOrgElement = orgElement.offset();
      leftOrgElement = coordsOrgElement.left;  
      widthOrgElement = orgElement.css('width');
      $('.cloned').css('left',leftOrgElement+'px').css('top',0).css('width',widthOrgElement).show();
      $('.original').css('visibility','hidden');
    } else {
      // not scrolled past the menu; only show the original menu.
      $('.cloned').hide();
      $('.original').css('visibility','visible');
    }
  }
}

App.changeLogoOnScroll = function() {

  var t = $('#hero').offset().top - 100;
  var t1 = $('#tagline').offset().top - 100;
  var t2 = $('#residences-amenities').offset().top - 100;
  var t3 = $('#neighborhood').offset().top - 100;
  var t4 = $('#contact').offset().top - 100;
  var t5 = $('#gmap').offset().top - 100;

  $(window).scroll(function() {

    var value = $(this).scrollTop();
    var white_logo = '/wp-content/themes/alta-spring-creek/assets/images/logo.svg';
    var gold_logo = '/wp-content/themes/alta-spring-creek/assets/images/spring-creek-gold.svg';

    if (value > t1 && value < t2) {
      $('.header-logo img').attr('src', gold_logo);
    } else if (value > t1 && value > t2 && value < t3) {
      $('.header-logo img').attr('src', white_logo);
    } else if (value > t3) {
      $('.header-logo img').attr('src', gold_logo);
    } else {
      $('.header-logo img').attr('src', white_logo);
    }
    
  });

}

// Instantiate functions when document is ready
$(document).ready(function() {
  // App.navLink();
  App.objectFitImg();
  App.smoothScroll();
  App.sliderInit();
  App.animatePlaceholders();
  App.promoClose();
  App.navLink();
  App.triggerVirtualTour();
  App.animateHomeBoxes();
  App.stickyNav();
  App.changeLogoOnScroll();
});

// Instantiate functions when document has loaded most assets such as images
$(window).on('load', function() {
  if ($('body').hasClass('home'))
    App.locationsMap();
  App.sliderFixedHeightImages();
  App.fadeAnimations();
  App.magnificLightboxImageGridInit();
});
