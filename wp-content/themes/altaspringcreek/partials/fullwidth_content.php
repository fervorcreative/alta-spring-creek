<?php

$content = get_sub_field('content');
$bottom_padding = get_sub_field( 'section_padding_button' );
?>

<section class="section fullwidth-content <?php padding_top_classes(); ?> <?php padding_bottom_classes($bottom_padding); ?>">
  <div class="grid-container full">
    <div class="grid-x text-center">
      <div class="cell small-12 medium-12 large-12">
        <?= $content; ?>
      </div> <!-- .cell --> 
    </div> <!-- .grid-x -->
  </div> <!-- .grid-container --> 
</section>