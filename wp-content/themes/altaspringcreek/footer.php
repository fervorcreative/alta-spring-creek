  </main>

  <footer id="footer" class="footer bg-black color-white" role="contentinfo">
    <div class="grid-container footer__info">
      <div class="grid-x align-middle align-center text-center">
        
        <div class="cell large-auto copyright">
        <?php 
          $address = get_field('address', 'option' );
          $city = get_field('city', 'option' );
          $state = get_field('state', 'option' );
          $zip = get_field('zip', 'option' );
          $phone = get_field( 'phone_number', 'option' );
          $call_number = str_replace( array('-', '.', '(', ')'), '', $phone );
          
          ?>  
          <p class="address"><?php echo $address; ?>, <?php echo $city; ?>, <?php echo $state; ?> <?php echo $zip; ?> <?php if( $phone ) { ?><span class="pipe hide-mobile">|</span> <span class="phone__number"><a href="tel:+<?php echo $call_number; ?>"><?php echo $phone; ?></a></span><?php } ?></p>
          <p class="footer__copyright">
            <span class="copyright">&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?></span> <span class="pipe hide-mobile">|</span> <span class="copyright__name">Professionally Managed by <a href="https://www.woodpartners.com/" target="_blank" rel="nofollow">Wood Residential</a> <span class="pipe hide-mobile">|</span></span> <span class="copyright__links"><a href="https://www.woodpartners.com/privacy-policy/" target="_blank">Privacy</a> <span class="pipe">|</span> <a href="https://www.woodpartners.com/digital-accessibility/?_ga=2.172898019.1930056624.1579276808-1612636055.1579276808" target="_blank">Digital Accessibility</a></span>
          </p> 
        </div>
      </div>
      <div class="grid-x grid-margin-x align-middle">
        <div class="cell small-12 text-center">
          <div class="wood-logos">
            <a href="https://www.woodpartners.com/" target="_blank"><img class="wood-logo" src="<?php echo get_template_directory_uri(); ?>/assets/images/wood-p.svg" alt="Wood Partners"></a>
            <a href="https://www.woodpartners.com/property-management/" target="_blank"><img class="wood-logo" src="<?php echo get_template_directory_uri(); ?>/assets/images/wood-r.svg" alt="Wood Residential Services"></a>
          </div>
          <div class="wood-logos">
            <img class="pet-logo" src="<?php echo get_template_directory_uri(); ?>/assets/images/wood-pets.svg" alt="We Love Pets">
            <img class="fair-logo" src="<?php echo get_template_directory_uri(); ?>/assets/images/equal-housing.svg" alt="Fair Housing">
            <img class="access-logo" src="<?php echo get_template_directory_uri(); ?>/assets/images/accessibility.svg" alt="Accessibility">
          </div>
          
        </div>
      </div>
      
      <div class="grid-x align-center text-center covid-disclosure">
        <div class="cell small-10 medium-9 large-9">
          <p>It’s up to each person to follow the CDC and health authority best practices to limit the spread of COVID-19. At Wood Residential Services, we have revised and enhanced our practices to help reduce the transmission of COVID-19 in the Community. When visiting or living at a Wood Residential Services community, you will see heightened cleaning measures, thoughtful amenity management plans, and flexible, virtual options to communicate with our management team.</p>
        </div> <!-- .cell -->
      </div>
    </div>
  </footer>
</div>

<?php wp_footer(); ?>

</body>
</html>
