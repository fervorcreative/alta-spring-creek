<?php
$hero = get_field( 'hero', get_the_ID() );
$h_img = $hero['image'];
$h_title = $hero['title'];
?>
<section id="hero" class="section hero">
  <div class="grid-container full">
    <div class="grid-x">
      <div class="cell">
        <div class="hero-box" style="background-image: url(<?php echo $h_img['url']; ?>);">
          <div class="hero-content">
            <h1 class="hero-title"><?php echo ($h_title ? $h_title : get_the_title() ) ?></h1>
          </div>
        </div>

      </div>
    </div>
  </div>
</section>