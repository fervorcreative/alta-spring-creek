/* global google, AppData, objectFitImages, AOS */

// Set up App object and jQuery
var App = App || {},
  $ = $ || jQuery;

App.objectFitImg = function() {
  objectFitImages();
}

App.navLink = function() {
  $('.js-nav-toggle').on( 'click', function() {
    // Show nav overlay
    if (!$('html').hasClass('is-open')) {
      $('#site-nav').fadeIn(200);
      $('html').addClass('is-open');
    // Hide nav overlay
    } else {
      $('#site-nav').fadeOut(250);
      window.setTimeout(function() {
        $('html').removeClass('is-open');
      }, 50);
    }
  });
  $('.js-nav-close-toggle').on('click', function () {
    $('#site-nav').fadeOut(250);
    window.setTimeout(function() {
      $('html').removeClass('is-open');
    }, 50);
  });
};

App.fadeAnimations = function() {
  AOS.init({
    disable: 'tablet',
    //offset: 200,
    duration: 1000,
    //once: true,
  });
}

App.smoothScroll = function() {
  $('a[href^=\\#]:not([href=\\#])').on('click', function(event) {
    var target = $.attr(this, 'href');
    var targetPosition = $(target).offset().top;
    var currentPosition = $('.site').offset().top;

    $('html, body').stop().animate({
        scrollTop: targetPosition - currentPosition
    }, 400);

    event.preventDefault();
  });
}

App.sliderInit = function() {
  // $('.alta-carousel').slick({
  //   centerMode: true,
  //   arrows: false,
  //   dots: true,
  //   centerPadding: '',
  //   slidesToShow: 1,
  //   adaptiveHeight: false,
  //   variableWidth: false,
  //   //prevArrow: $('.prev'),
  //   //nextArrow: $('.next'),
  // });
  var $slider = $('.image-slider').flickity({
    cellAlign: 'center',
    contain: true,
    pageDots: false,
    prevNextButtons: false,
    wrapAround: true
  });
  var $slider_stats = $('.slider-stats');
  var flkty = $slider.data('flickity');
  var $prev = $('.image-carousel').find('.prev');
  var $next = $('.image-carousel').find('.next');
  // previous
  $prev.on('click', function () {
    $slider.flickity('previous');
  });

  // next
  $next.on('click', function () {
      $slider.flickity('next');
  });

  
  function updateStats() {
    var cellNumber = flkty.selectedIndex + 1;
    var slideTotal = flkty.slides.length;
    
    var cellNumber_pad = (cellNumber < 10) ? ("0" + cellNumber) : cellNumber;
    var slideTotal_pad = (slideTotal < 10) ? ("0" + slideTotal) : slideTotal;
    $slider_stats.text( cellNumber_pad + '/' + slideTotal_pad);
  }
  if ($slider.length) {
    updateStats();
  }
  
  $slider.on('change.flickity', updateStats);
  
  function padNumber(number, length) {
    var str = '' + number;
    while (str.length < length) {
      str = '0' + str;
    }
    return str;
  }
}

App.sliderFixedHeightImages = function() {
  $('.alta-carousel').on('setPosition', function () {
    AltaResizeSlider();
  });
   
  //we need to maintain a set height when a resize event occurs.
  //Some events will through a resize trigger: $(window).trigger('resize');
  $(window).on('resize', function(e) {
    AltaResizeSlider();
  });
   
  //since multiple events can trigger a slider adjustment, we will control that adjustment here
  function AltaResizeSlider(){
    $slickSlider = $('.alta-carousel');
    $slickSlider.find('.slick-slide').height('auto');
   
    var slickTrack = $slickSlider.find('.slick-track');
    var slickTrackHeight = $(slickTrack).height();
   
    $slickSlider.find('.slick-slide').css('height', slickTrackHeight + 'px');
  }
}
App.floorPlans = function () {
  
  var requestType = 'floorplan',
    apiToken = 'd1eaffc8-ec15-4be1-9e5e-d2b393f3b9eb',
    propertyCode = 'p1189627',
    apiURL = 'https://api.rentcafe.com/rentcafeapi.aspx?requestType=' + requestType + '&apiToken=' + apiToken + '&propertyCode=' + propertyCode + '';
  if ( $('.floor-plan-units').length ) {
    fetchFloorPlans();
  }
  
  
  function fetchFloorPlans() {
    var filter = $('.fp-filter');
    var showData = $('.floor-plan-units');
    
    $.getJSON(apiURL, function (data) {
      showData.empty();
      
      var content = '';
      var filterItems = '';
      var units = new Array();
      
      for (var i = 0; i < data.length; i++) {
        var item = data[i];
        
        // set up data
        var FloorplanName = item.FloorplanName,
          Beds = item.Beds,
          Baths = item.Baths,
          MinimumSQFT = item.MinimumSQFT,
          MinimumRent = item.MinimumRent,
          MaximumRent = item.MaximumRent,
          AvailabilityURL = item.AvailabilityURL,
          floorPlanNameSlug = FloorplanName.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '-'),
          imgURL = item.FloorplanImageURL,
          unitName = '',
          slugName = '',
          priceText;
        if (MinimumRent < 0) {
          priceText = 'Call for Pricing: <a class="phone dniphonehref" href="tel:+18337567561"><span class="dniphone">833.756.7561</span></a>'
        } else {
          priceText = 'Starting at $' + MinimumRent;
        }
        
        if( Beds == 0 ) {
          unitName = 'Studio';
          slugName = 'studio';
        }
        if(Beds == 1 ) {
          unitName = '1 Bedroom';
          slugName = 'one-bedroom';
        }
        if( Beds == 2 ) {
          unitName = '2 Bedrooms';
          slugName = 'two-bedrooms';
        }
        if( Beds == 3 ) {
          unitName = '3 Bedrooms';
          slugName = 'three-bedrooms';
        }
        if( $.inArray(data[i].Beds, units) == -1) units.push(data[i].Beds);
        
        content += '<div class="fp-wrapper mix '+ slugName +'">';
          content += '<div class="fp-details align-middle">';
            content += '<div class="fp-name"><h4>'+ FloorplanName +'</h4></div>';
            content += '<div class="fp-bath"><p>Bath: '+ Baths +'</p></div>';
            content += '<div class="fp-room"><p>'+ MinimumSQFT +' sq. ft.</p></div>';
            content += '<div class="fp-price"><p>'+ priceText +'</p></div>';
            content += '<div class="fp-btn"><a href="#floor-plan-'+ floorPlanNameSlug +'" class="button" data="view-plan">View Floor Plan</a></div>';
          content += '</div>';
          content += '<div id="floor-plan-'+ floorPlanNameSlug +'" class="fp-fullview">';
          content += '<div class="fp-fullview--name">';
            content += '<h3>Floor Plan '+ FloorplanName +'</h3>';
            content += '<p>' + unitName + '</p>';
            content += '<p><a href="'+ AvailabilityURL +'" class="button">Apply Now</a></p>';
            content += '<p><a href="https://altaspringcreek.securecafe.com/onlineleasing/alta-spring-creek/scheduletour.aspx" class="button" target="_blank">Schedule a Tour</a></p>';
          content += '</div>';
          content += '<div class="fp-fullview--image"><a href="'+ imgURL +'" class="mfp-image"><img src="'+ imgURL +'" alt="Floorplan Image"></a></div>';
          //content += '<div class="fp-fullview--btn"></div>';
        content += '</div></div>';
      }
      
      showData.append(content);
      for( var j=0; j<units.length; j++ ) {
				var unitName = ''
					slugName = '';

				if( units[j] == 0 ) {
					unitName = 'Studio';
 				   	slugName = 'studio';
				}
				if(units[j] == 1 ) {
					unitName = '1 Bedroom';
					slugName = 'one-bedroom';
				}
				if( units[j] == 2 ) {
					unitName = '2 Bedrooms';
					slugName = 'two-bedrooms';
				}
				if( units[j] == 3 ) {
					unitName = '3 Bedrooms';
					slugName = 'three-bedrooms';
				}

				filterItems += '<li><a href="javascript:;" title="'+ unitName +'" data-filter=".'+ slugName +'">'+ unitName +'</a></li>';
			}
			filter.append(filterItems);
    }).done(function () {
      
      var mixer = mixitup('.floor-plan-units', {
          animation: {
          effects: 'fade translateZ(-100px)'
        }
      });
      $('.mfp-image').magnificPopup({
				type: 'image',
				mainClass: 'mfp-fade',
				gallery: {
					enabled: false,
					navigateByImgClick: true,
					preload: [0,1] // Will preload 0 - before current, and 1 after the current image
				}
      });
      
      var defaultText = 'View Floor Plan';
      $('a[data="view-plan"]').on('click', function (e) {
        e.preventDefault();
        $('html, body').stop();
        var target = $(this).attr('href');

        $(target).toggleClass('expand');
        $(this).toggleClass('is-active');

        if ($(this).hasClass('is-active')) {
          $(this).text("Hide Floor Plan");
        } else {
          $(this).text(defaultText);
        }

      });
    }).fail(function () {
      var error_message = '<p>We had trouble loading the floor plans. Please try again later.</p>';
      showData.html(error_message);
    });
    
    showData.html('<img src="'+ AppData.template_dir +'/assets/images/loader.svg" alt="Loading. Please wait">');
  }
  
  
}
App.magnificLightboxImageGridInit = function () {
  
  $('.gallery-grid').magnificPopup({
    delegate: 'a',
    type: 'image',
    removalDelay: 500,
    gallery: {
      enabled: true,
      navigateByImgClick: true,
      preload: [0,1]
    },
    image: {
      tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
    },
    callbacks: {
      elementParse: function (item) {
        if (item.el[0].className == 'js-video') {
          item.type = 'iframe';
        } else {
          item.type = 'image';
        }
      },
    },
    midClick: true,
    gallery: {
      enabled: true,
      //arrowMarkup: '<button title="%title%" type="button" class="mfp-btn mfp-btn-%dir%">%title%</button>', // markup of an arrow button,
      tPrev: 'Previous', // title for left button
      tNext: 'Next', // title for right button
    },
  });
  
  $('.js-zoom').magnificPopup({
    type: 'image'
  });
  // $('.js-gallery').magnificPopup({
  //   type: 'image',
  //   gallery: {
  //     enabled: true
  //   }
  // });
  // $('.js-video').magnificPopup({
  //   type: 'iframe'
  // });
  
  $('.js-inline').on('click', function (e) {
    e.preventDefault();
    var target = $(this).data('href');
    //console.log(target);
    $.magnificPopup.open({
      type: 'inline',
      tLoading: 'Loading...',
      items: {
        src: target
      }
    });
  });
}

App.promoBarToggle = function() {
  var timeInHours = .25
  var expirationDuration = 1000 * 60 * 60 * timeInHours;
  var prevAccepted = localStorage.getItem('promoShown');
  var currentTime = new Date().getTime();
  var notAccepted = prevAccepted === undefined;
  var prevAcceptedExpired = prevAccepted !== undefined && currentTime - prevAccepted > expirationDuration
  if (notAccepted || prevAcceptedExpired) {
    localStorage.setItem('promoShown', currentTime);
  } else {
    $('#promo-bar').hide();
  }
} //function

App.promoClose = function() {
  $('.promo__close').on('click', function() {
    $('#promo-bar, .promo__close').slideUp(250, function() {
      $('header').css('top', '0');
    });
    $('#logo').removeClass('hidden');
  });
  
}

function getCurrentScroll() {
  return window.pageYOffset || document.documentElement.scrollTop;
}
App.stickyHeader = function () {
  
  //console.log('scroll: ' + scroll + '. Header pos: ' + headerPos);
  $(window).on('scroll', function (e) {
    var scroll = getCurrentScroll();
    var headerPos = $('.header').height();
    //console.log('Scroll Pos: ' + scroll);
    if (scroll > 400) { 
      $('.header').addClass('fixed-header');
    } else {
      $('.header').removeClass('fixed-header');
    }
  });
}
App.stickyNav = function() {
  // Create a clone of the menu, right next to original
  //$('.header').addClass('original').clone().insertAfter('.header').stop(true,true).addClass('cloned bg-beige', 500).css('position','fixed').css('top','0').css('margin-top','0').css('z-index','500').removeClass('original').hide();

  scrollIntervalID = setInterval(stickIt, 10);

  function stickIt() {
  
  var orgElementPos = $('.header').offset();
    orgElementTop = orgElementPos.top;               

    if ($(window).scrollTop() > (orgElementTop)  ) {
      // scrolled past the original position; now only show the cloned, sticky element.

      // Cloned element should always have same left position and width as original element.     
      orgElement = $('.header');
      coordsOrgElement = orgElement.offset();
      leftOrgElement = coordsOrgElement.left;  
      widthOrgElement = orgElement.css('width');
      //$('.cloned').css('left',leftOrgElement+'px').css('top',0).css('width',widthOrgElement).show();
      //$('.original').css('visibility','hidden');
      $('.header').addClass('fixed-header');
    } else {
      // not scrolled past the menu; only show the original menu.
      $('.header').removeClass('fixed-header');
      //$('.cloned').hide();
      //$('.original').css('visibility','visible');
    }
  }
}

App.triggerVirtualTour = function() {
  $('.virtual-tour-link a').addClass('fancybox-iframe');
}


App.googleMap = function () {
  var lat = 32.971563;
  var lng = -96.657187;
  var mapOptions = {
    zoom: 15,
    scrollwheel: false,
    saturation: -100,
    center: new google.maps.LatLng(lat, lng),
    disableDefaultUI: true,
    styles: [
      {
          "featureType": "administrative",
          "elementType": "labels.text.fill",
          "stylers": [
              {
                  "color": "#444444"
              }
          ]
      },
      {
          "featureType": "landscape",
          "elementType": "all",
          "stylers": [
              {
                  "color": "#f2f2f2"
              }
          ]
      },
      {
          "featureType": "poi",
          "elementType": "all",
          "stylers": [
              {
                  "visibility": "off"
              }
          ]
      },
      {
          "featureType": "road",
          "elementType": "all",
          "stylers": [
              {
                  "saturation": -100
              },
              {
                  "lightness": 45
              }
          ]
      },
      {
          "featureType": "road.highway",
          "elementType": "all",
          "stylers": [
              {
                  "visibility": "simplified"
              }
          ]
      },
      {
          "featureType": "road.highway",
          "elementType": "geometry.fill",
          "stylers": [
              {
                  "color": "#ffffff"
              }
          ]
      },
      {
          "featureType": "road.arterial",
          "elementType": "labels.icon",
          "stylers": [
              {
                  "visibility": "off"
              }
          ]
      },
      {
          "featureType": "transit",
          "elementType": "all",
          "stylers": [
              {
                  "visibility": "off"
              }
          ]
      },
      {
          "featureType": "water",
          "elementType": "all",
          "stylers": [
              {
                  "color": "#dde6e8"
              },
              {
                  "visibility": "on"
              }
          ]
      }
    ]
  }
  var map = new google.maps.Map(document.getElementById('gmap'), mapOptions);
  var markerImage = {
    url: AppData.template_dir + '/assets/images/map-markers/marker-home.png',
    //size: new google.maps.Size(50, 50),
    scaledSize: new google.maps.Size(50,50),
  };
  var myLatLng = new google.maps.LatLng(lat,lng);
  var mapMarker = new google.maps.Marker({
    position: myLatLng,
    map: map,
    icon: markerImage,
    url: '',
    anchor: new google.maps.Point(24, 24)
  });
  google.maps.event.addListener(mapMarker, 'click', function() {
    if (this.url) {
      window.open(this.url, '_blank');
    }
  });
}
App.locationsMap = function () {
  var lat = 32.971563;
  var lng = -96.657187;
  var zoomLevel = 14;
  
  var map_trigger = $('[data-location-id]'),
      filter_trigger = $('[data-category]'),
      map_options = {
          center: new google.maps.LatLng(lat, lng),
          mapTypeControl: false,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          navigationControl: false,
          navigationControlOptions: {
              style: google.maps.NavigationControlStyle.DEFAULT
          },
          scrollwheel: false,
          streetViewControl: false,
          zoom: zoomLevel,
          panControl: false,
          styles: [
              {
                  'featureType': 'water',
                  'elementType': 'geometry',
                  'stylers': [
                      {
                          'color': '#e9e9e9'
                      },
                      {
                          'lightness': 17
                      }
                  ]
              },
              {
                  'featureType': 'landscape',
                  'elementType': 'geometry',
                  'stylers': [
                      {
                          'color': '#f5f5f5'
                      },
                      {
                          'lightness': 20
                      }
                  ]
              },
              {
                  'featureType': 'road.highway',
                  'elementType': 'geometry.fill',
                  'stylers': [
                      {
                          'color': '#ffffff'
                      },
                      {
                          'lightness': 17
                      }
                  ]
              },
              {
                  'featureType': 'road.highway',
                  'elementType': 'geometry.stroke',
                  'stylers': [
                      {
                          'color': '#ffffff'
                      },
                      {
                          'lightness': 29
                      },
                      {
                          'weight': 0.2
                      }
                  ]
              },
              {
                  'featureType': 'road.arterial',
                  'elementType': 'geometry',
                  'stylers': [
                      {
                          'color': '#ffffff'
                      },
                      {
                          'lightness': 18
                      }
                  ]
              },
              {
                  'featureType': 'road.local',
                  'elementType': 'geometry',
                  'stylers': [
                      {
                          'color': '#ffffff'
                      },
                      {
                          'lightness': 16
                      }
                  ]
              },
              {
                  'featureType': 'poi',
                  'elementType': 'geometry',
                  'stylers': [
                      {
                          'color': '#f5f5f5'
                      },
                      {
                          'lightness': 21
                      }
                  ]
              },
              {
                  'featureType': 'poi.park',
                  'elementType': 'geometry',
                  'stylers': [
                      {
                          'color': '#dedede'
                      },
                      {
                          'lightness': 21
                      }
                  ]
              },
              {
                  'elementType': 'labels.text.stroke',
                  'stylers': [
                      {
                          'visibility': 'on'
                      },
                      {
                          'color': '#ffffff'
                      },
                      {
                          'lightness': 16
                      }
                  ]
              },
              {
                  'elementType': 'labels.text.fill',
                  'stylers': [
                      {
                          'saturation': 36
                      },
                      {
                          'color': '#333333'
                      },
                      {
                          'lightness': 40
                      }
                  ]
              },
              {
                  'elementType': 'labels.icon',
                  'stylers': [
                      {
                          'visibility': 'off'
                      }
                  ]
              },
              {
                  'featureType': 'transit',
                  'elementType': 'geometry',
                  'stylers': [
                      {
                          'color': '#f2f2f2'
                      },
                      {
                          'lightness': 19
                      }
                  ]
              },
              {
                  'featureType': 'administrative',
                  'elementType': 'geometry.fill',
                  'stylers': [
                      {
                          'color': '#fefefe'
                      },
                      {
                          'lightness': 20
                      }
                  ]
              },
              {
                  'featureType': 'administrative',
                  'elementType': 'geometry.stroke',
                  'stylers': [
                      {
                          'color': '#fefefe'
                      },
                      {
                          'lightness': 17
                      },
                      {
                          'weight': 1.2
                      }
                  ]
              }
          ]
      },
      map = new google.maps.Map(document.getElementById('location-map'), map_options),
      infowindow = new google.maps.InfoWindow(),
      markers = [],
      infowindows = [];
  
  map_trigger.on('click', function () {
      var location_id = parseInt($(this).attr('data-location-id'));
      
      infowindow.close();
      infowindow.setContent(infowindows[location_id]);
      infowindow.open(map, markers[location_id]);
      map.panTo(markers[location_id].getPosition());
      //markers[location_id].setZIndex(google.maps.Marker.MAX_ZINDEX + 1);
    /*
      $('html,body').stop().animate({
          'scrollTop': $('.neighborhood-map').offset().top - 40
      }, 1000, 'swing');
    */

  });
  /*
  map_trigger.click( function(e) {
      e.preventDefault();

      var location_id = parseInt( $(this).attr('data-location-id') );

      infowindow.close();
      infowindow.setContent( infowindows[location_id] );
      infowindow.open( map, markers[location_id] );
      map.panTo(markers[location_id].getPosition());
      markers[location_id].setZIndex(google.maps.Marker.MAX_ZINDEX + 1);

      $('html,body').stop().animate({
          'scrollTop': $('#location-map').offset().top - 200
      }, 400, 'swing');
  });
  */
  // Trigger filter on click
  filter_trigger.on('click', function(e) {
      e.preventDefault();
      $('.filter-items li a').removeClass( 'is-active' );
      //var $locations = $('.map__locations > li'),
      var	category = $(this).attr('data-category');

      if( $(this).hasClass( 'is-active' ) ) {
          category = '';
      }


      filterLocations( category );

      // Change class on filters
      //$('.cat-btn').removeClass('is-active');
      $(this).addClass('is-active');

      // Change class on locations
      /*$locations.removeClass('is-active');
      $locations.parent().find('.map__locations-' + category).addClass('is-active');*/

      // Update dropdown
      //filter_dropdown.val( category );
      
      // scroll window to map
      // $('html,body').stop().animate({
      //     'scrollTop': $('#map').offset().top - 200
      // }, 400, 'swing' );
      
  });

  function filterLocations( category ) {
      console.log( category );
      var bounds = new google.maps.LatLngBounds(), i;

      // Defaults category to ???? for manual trigger
      if ( !category ) {
          category = 'all-categories';
      }


      

      // Go through and hide/show markers while calculating the bounds
      for ( i = 0; i < markers.length; i++ ) {

          if ( markers[i] ) {
              if(markers[i].category === 'home' || category === 'all-categories' ) {
                  markers[i].setMap(map);
                  bounds.extend(markers[i].getPosition());

              } else if ( markers[i].category !== category ) {
                  markers[i].setMap(null);
                  //console.log( 'flag this area');
              } else {
                  markers[i].setMap(map);
                  bounds.extend(markers[i].getPosition());
              }
          }
      }
      // Set map bounds
      map.setCenter(bounds.getCenter());
      map.fitBounds(bounds);
      // Remove one zoom level to ensure no marker is on the edge.
      map.setZoom(map.getZoom());

  };
  /*
  // set home marker above all markers
  new google.maps.Marker({
      position: new google.maps.LatLng(lat, lng),
      map: map,
      title: "Alta Union House",
      icon: AppData.template_dir + '/images/map-icons/marker-home.png',
      zIndex: google.maps.Marker.MAX_ZINDEX + 1
  });
  */
  //console.log( AppData.template_dir + '/images/map-icons/icon-art-and-culture.png' );
  for ( var i = 0; i < AppData.locations.length; i += 1 ) {
    (function () {
      var location = AppData.locations[i],
      latitude = location.custom_fields._wpseo_coordinates_lat,
      longitude = location.custom_fields._wpseo_coordinates_long,
      latLng = new google.maps.LatLng(latitude, longitude),
      icon = {
        //url: AppData.template_dir + '/assets/images/map-icons/marker-home.svg',
        url: AppData.template_dir + '/assets/images/map-markers/marker-' + location.category.slug + '.svg',
        //url: AppData.template_dir + '/assets/images/map-markers/marker.svg',
        //url: AppData.template_dir + '/images/map-icons/marker.png',
        //scaledSize: new google.maps.Size(50, 60),
        //origin: new google.maps.Point(0,0), // origin
        //anchor: new google.maps.Point(24, 48) // anchor
      },
      marker = new google.maps.Marker({
        map: map,
        title: location.post_title,
        position: latLng,
        zIndex: 1,
        category: location.category.slug
      }),
      content = [
        '<div class="infowindow">',
        '<strong>' + location.post_title + '</strong><br>',
        '<span>' + location.custom_fields._wpseo_business_address + '</span><br>',
        '<span>' + location.custom_fields._wpseo_business_city + ', ' + location.custom_fields._wpseo_business_state + ' ' + location.custom_fields._wpseo_business_zipcode + '</span><br>',
        //'<span>'+ location.custom_fields._wpseo_business_phone +'</span>',
        //'<span>' + '<a href="http://' + location.custom_fields._wpseo_business_url + '" target="_blank">View Website</a>' + '</span>',
        '</div>'
      ].join('');

      markers[location.ID] = marker;
      infowindows[location.ID] = content;

      marker.setIcon(icon);

      google.maps.event.addListener( marker, 'click', function() {
          infowindow.close();
          infowindow.setContent(content);
          infowindow.open(map, this);
          this.setZIndex(google.maps.Marker.MAX_ZINDEX + 1);
      });

      google.maps.event.addDomListener(window, 'resize', function() {
          var mapCenter = map.getCenter();
          google.maps.event.trigger(map, 'resize');
          map.setCenter(mapCenter);
      });

    }());
  }
}

App.animatePlaceholders = function() {
  $('input, select').focus(function(){
    placeholder = $(this).attr('placeholder');
    if(placeholder != undefined){
      $(this).parent().prepend('<span class="input-placeholder">'+placeholder+'</span>');
      $(this).attr('placeholder', '');
    }
  });

  $('input').blur(function(){
    $(this).attr('placeholder', $('.input-placeholder').text());
    $(this).parent().find('.input-placeholder').remove();
  });
}
App.floatingLabels = function () {
  $('.gform_wrapper .gfield input, .gform_validation_error input').focus( function() {
    var $this = $(this);
    $this.parent().prev('.gfield_label').addClass('show-my-label');
  }).on('blur', function () {
    var $this = $(this);
    if (!$this.val() ||  $this.val() === '' ) {
      $this.parent().prev('.gfield_label').removeClass('show-my-label');
    }
  });
}

App.animateHomeBoxes = function() {
  $('.box-image').on('touchstart', function() {
    //alert('Touchstart!');
    //Used this function to add touchstart event, which allows Safari to operate with CSS animations
  })
}

App.virtualTours = function () {
  if ($('.virtual-tour-units').length ) {
    var mixer = mixitup('.virtual-tour-units', {
      animation: {
        effects: 'fade translateY(-100px)'
      }
    });
    
    
  }
}
App.tourForm = function () {
  //var disabledDays = [0, 6];
  $('#movin-date').datepicker({
    minDate: new Date(),
    language: 'en'
  });
  $('#date-field').datepicker({
    class: 'date-field',
    minDate: new Date(),
    /*
    onRenderCell: function (date, cellType) {
      if (cellType == 'day') {
        var day = date.getDay(),
            isDisabled = disabledDays.indexOf(day) != -1;

        return {
          disabled: isDisabled
        }
      }
    },*/
    language: 'en',
    onSelect: function onSelect(date) {
      console.log('User has chosen: ' + date);
      loadTimeSlots(date);
    }
  });
  
  function loadTimeSlots(chosen_date) {
    var months = [
      'January',
      'February',
      'March',
      'April',
      'May',
      'June',
      'July',
      'August',
      'September',
      'October',
      'November',
      'December'
    ];
    if ( ! $('#timeslots').hasClass('hidden') ) {
      $('#timeslots').addClass('hidden');
    }
    var selected_date = new Date(chosen_date);
    
    var api_url = 'https://marketingapi.rentcafe.com/marketingapi/api/appointments/AvailableSlots',
        MarketingAPIKey = 'b6b2d83f-d9d6-4f89-937e-ddb474a740b1',
        CompanyCode = 'c00000006857',
        PropertyCode = 'p1189627',
        tMonth = selected_date.getMonth(),
        tDay = selected_date.getDate();
    var api_test = 'http://altaspringcreek.local/wp-content/themes/altaspringcreek/apitest.json';
    function pad(n) {
        return (n < 10) ? '0' + n : n;
    }
    $.ajax({
      url: api_url,
      type: 'POST',
      data: {
        MarketingAPIKey: MarketingAPIKey,
        CompanyCode: CompanyCode,
        PropertyCode: PropertyCode
      },
      dataType: 'JSON',
      beforeSend: function() {
        $('#timeslots').html('');
        if (! $('.loader').hasClass('.hidden') ) {
          $('.loader').removeClass('hidden');
        }
        $('.loader').html('<img src="' + AppData.template_dir + '/assets/images/puff.svg" alt="Loading timeslots. Please wait">');
      },
      success: function (data, status, jQxhr) {
        $('#timeslots').removeClass('hidden');
        $('.loader').addClass('hidden').html('');
        //console.log( data.Response.0.AvailableSlots );
        //console.log( data );
        //modelHTML += '<select id="timeslots" class="dropdown>';
        var successes = [];
        $.each(data.Response[0], function(index, value) {
          //now you can access properties using dot notation
          //console.log( 'index: ' + index + '. Value: ' + value );
          $.each( value, function(i, v ) {
            //if(i>10) return false;
            console.log( v );
            console.log(v.TypeofSlot);
            var startTime = new Date(v.dtStart),
              //endTime = v.dtEnd,
              stMonth = startTime.getMonth(),
              stDay = startTime.getDate();
            // stHour = startTime.getHours(),
            // stMin = startTime.getMinutes(),
            // tourType = v.TypeofSlot;
            // var tourType_text = ''
            // if (tourType == 'GuidedTour') {
            //   tourType_text = 'Guided Tour';
            // } else if(tourType == 'Personal') {
            //   tourType_text = 'Personal Tour';
            // } else if(tourType == 'VirtualTour') {
            //   tourType_text = 'Virtual Tour';
            // }
            if (stMonth === tMonth) {
              if (stDay === tDay) {
                  successes.push({ starttime : v.dtStart, tourtype: v.TypeofSlot });
              }
            }

            // if( stHour > 12 ) {
            //   stHour -= 12;
            // } else if( stHour === 0) {
            //   stHour = 12;
            // }
            //var displayHours = pad(stHour) + ':' + pad(stMin);
            // if( stMonth === tMonth && stDay === tDay ) {
            //   $('#timeslots').append('<option value="'+startTime.toLocaleTimeString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true})+'">'+startTime.toLocaleTimeString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true}) +' (' + tourType_text +')</option>');   
            // }
            //$('#appttype').val(tourType_text).removeClass( 'hidden' );
            //console.log( 'Start Month: '+ stMonth +'. Test Month: ' + tMonth );
          });
        });
        if (successes.length) {
          // console.log('Days that match: ' + successes.length);
          //console.dir(successes);
          for (var j = 0; j < successes.length; j++) {
            //console.log('index: ' + j + '. Value: ' + successes[j]);
            //console.log( successes[j].starttime );
            var start_time = new Date( successes[j].starttime ),
                tourType = successes[j].tourtype,
                tourType_text = '';
            if (tourType === 'GuidedTour') {
              tourType_text = 'Guided Tour';
            } else if(tourType === 'Personal') {
              tourType_text = 'Personal Tour';
            } else if(tourType === 'VirtualTour') {
              tourType_text = 'Virtual Tour';
          }
          $('#timeslots').append('<option value="' + start_time.toLocaleTimeString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true }) + '">' + start_time.toLocaleTimeString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true }) +' ('+ tourType_text +')</option>'); 
          /*
          if (typeof successes[j] !== 'undefined') {

          //console.log( 'index: ' + j + '. Value: ' + successes[j] );
          var start_time = new Date(successes[j]);
          $('#timeslots').append('<option value="' + start_time.toLocaleTimeString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true }) + '">' + start_time.toLocaleTimeString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true }) +'</option>'); 
          }
          */
          }

        } else {
          $('#timeslots').html('').addClass('hidden');
          $('.loader').removeClass('hidden').html('<p>There are no time slots available for '+ months[tMonth] +' '+ tDay +'</p>');
        }
        //$('#test').html(modelHTML);
      },
      error: function (errorMessage) { // error callback 
        $('.loader').html('<p>Error: ' + errorMessage + '</p>');
      }
    });
  }
  $('.gf_tour_form').validate({
    rules: {
      ApptDate: 'required',
      ApptTime: 'required',
      FirstName: 'required',
      LastName: 'required',
      Email: {
        required: true,
        email: true
      },
      Phone: {
        required: true,
        phoneUS: true
      }
    },
    errorClass: 'gfield_description',
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.insertAfter( element.parent() );
    },
    submitHandler: function (form) {
      //var addlData = {'MarketingAPIKey':'b6b2d83f-d9d6-4f89-937e-ddb474a740b1','CompanyCode':'C00000006857','PropertyCode': 'p1189627'}
      var serializeData = $(form).serialize();
      $.ajax({
        url: 'https://marketingapi.rentcafe.com/marketingapi/api/appointments/createleadwithappointment?MarketingAPIKey=b6b2d83f-d9d6-4f89-937e-ddb474a740b1&CompanyCode=C00000006857&PropertyCode=p1189627',
        type: 'POST',
        data: serializeData,
        success: function (data) {
          console.log('data: ' + data);
          $('#form-messages').html('<p>Your tour request has been submitted. You will be hearing from us soon!</p>');
          $('.gf_tour_form').hide();
        },
        error: function (errorMessage) { // error callback 
          //$('#form-messages').html('<p>Error: ' + errorMessage + '</p>');
          console.log('Error: ' + errorMessage);
        }
      });
    }
  });
}
App.changeLogoOnScroll = function() {

  var t = $('#hero').offset().top - 100;
  var t1 = $('#tagline').offset().top - 100;
  var t2 = $('#residences-amenities').offset().top - 100;
  var t3 = $('#neighborhood').offset().top - 100;
  var t4 = $('#contact').offset().top - 100;
  var t5 = $('#gmap').offset().top - 100;

  $(window).scroll(function() {

    var value = $(this).scrollTop();
    var white_logo = '/wp-content/themes/alta-spring-creek/assets/images/logo.svg';
    var gold_logo = '/wp-content/themes/alta-spring-creek/assets/images/spring-creek-gold.svg';

    if (value > t1 && value < t2) {
      $('.header-logo img').attr('src', gold_logo);
    } else if (value > t1 && value > t2 && value < t3) {
      $('.header-logo img').attr('src', white_logo);
    } else if (value > t3) {
      $('.header-logo img').attr('src', gold_logo);
    } else {
      $('.header-logo img').attr('src', white_logo);
    }
    
  });

}

App.customWindow = function () {
  $('.custom-window').on('click', function (e) {
    e.preventDefault();
		var target = $(this).attr('href');
		var params = 'scrollbars=no,resizable=no,status=no,location=no,toolbar=no,menubar=no,width=530,height=570';
		window.open( target, 'Chat', params );
  });
}

// Instantiate functions when document is ready
$(function () {
  
  App.navLink();
  App.objectFitImg();
  App.smoothScroll();
  App.sliderInit();
  //App.animatePlaceholders();
  App.floatingLabels();
  App.promoClose();
  //App.triggerVirtualTour();
  App.animateHomeBoxes();
  //App.stickyNav();
  App.stickyHeader();
  App.customWindow();
  //App.changeLogoOnScroll();
});

// Instantiate functions when document has loaded most assets such as images
$(window).on('load', function() {
  if ( $('body').hasClass('page-id-6') ) { App.locationsMap(); }
    
  if ( $('body').hasClass('home') || $('body').hasClass('page-id-8')) { App.googleMap(); }
    
  App.sliderFixedHeightImages();
  App.fadeAnimations();
  App.magnificLightboxImageGridInit();
  App.floorPlans();
  App.virtualTours();
  App.tourForm();
});
