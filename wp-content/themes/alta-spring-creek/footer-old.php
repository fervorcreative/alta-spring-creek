<footer class="footer bg-black color-white" role="contentinfo">
    <div class="grid-container">
      <div class="grid-x">
        <div class="footer__text cell text-center">Alta Potrero <span>|</span><br> 1301 16th Street, San Francisco, CA 94103</div>
      </div>
      <div class="grid-x align-middle">
        <div class="cell large-auto">
          <p class="footer__copyright">
            &copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?>
            <span>|</span>
            Professionally Managed by Wood Residential Services
            <span>|</span>
            <a href="https://www.woodpartners.com/privacy-policy/" target="_blank">Privacy Policy</a>
          </p>
        </div>
        <div class="footer__icons cell large-shrink">
          <a href="https://www.woodpartners.com/" target="_blank">
            <img class="footer__icon-p" src="<?php bloginfo('template_directory'); ?>/assets/images/wood-p@2x.png" alt="Wood Partners">
          </a>
          <a href="https://www.woodpartners.com/property-management" target="_blank">
            <img class="footer__icon-r" src="<?php bloginfo('template_directory'); ?>/assets/images/wood-r@2x.png" alt="Wood Partners Residential Services">
          </a>
          <a href="https://www.woodpartners.com/wp-content/uploads/2017/11/WRS-Pet-Policy.pdf" target="_blank">
            <img class="footer__icon-pets" src="<?php bloginfo('template_directory'); ?>/assets/images/wood-pets-logo@2x.png" alt="WRS Loves Pets">
          </a>
          <a href="https://www.hud.gov/program_offices/fair_housing_equal_opp" target="_blank">
            <img class="footer__icon-f" src="<?php bloginfo('template_directory'); ?>/assets/images/f-icons@2x.png" alt="Equal Housing and Accessibility">
          </a>
        </div>
      </div>
    </div>
  </footer>
</div>

<?php wp_footer(); ?>

</body>
</html>
