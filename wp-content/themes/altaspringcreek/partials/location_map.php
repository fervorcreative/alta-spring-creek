<?php
$offset_map = get_sub_field( 'offset_map'); // boolean
$padding_top = get_sub_field( 'section_padding_top' );
$bottom_padding = get_sub_field( 'section_padding_bottom' );
?>
<section class="section location-map <?php echo ($offset_map ? 'offset-map' : ''); ?> <?php padding_top_classes(); ?> <?php padding_bottom_classes($bottom_padding); ?>">
    <div class="grid-container">
        <div class="grid-x align-center align-middle">
            <div class="cell large-10">
            <?php
                $terms = get_terms(array(
                    'taxonomy' => 'wpseo_locations_category',
                    'exclude' => array( 2 ),
                    'hide_empty' => 1,
                    'orderby' => 'slug',
                ));
                ?>
                <ul class="filter-items no-bullets map-filter">
                    <li><a href="javascript:;" class="cat-all is-active" data-category="all-categories">All</a></li>
                    <?php
                    //var_dump( $terms );
                    foreach ( $terms as $category ) {

                        $locations = get_locations( $category );
                        $cat_title = $category->name;
                        $cat_nicename = $category->slug; ?>
                        <li>
                            <a href="javascript:;" class="cat-<?php echo $cat_nicename; ?>" data-category="<?php echo $cat_nicename; ?>">
                                <?php echo $cat_title; ?>
                            </a>
                        </li>
                    <?php } ?>
                </ul>
            </div>
            <div class="cell large-10">
                <div class="mapHolder">
                    <div id="location-map">
                        ...finding locations
                    </div>
                </div>
                
                
            </div>
        </div>
    </div>
</section>