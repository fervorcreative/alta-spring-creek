<?php

$content = get_sub_field('content');

?>

<section class="section fullwidth-content<?php padding_top_classes(); ?><?php padding_bottom_classes(); ?>">
  <div class="grid-container full">
    <div class="grid-x text-center">
      <div class="cell small-12 medium-12 large-12">
        <?= $content; ?>
      </div> <!-- .cell --> 
    </div> <!-- .grid-x -->
  </div> <!-- .grid-container --> 
</section>