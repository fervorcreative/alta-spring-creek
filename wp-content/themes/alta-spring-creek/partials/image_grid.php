<?php

$grid_images = get_sub_field('grid_images');
$bg_color = get_sub_field('background_color'); ?>

<section class="section image-grid" <?= (!empty($bg_color) ? 'style="background-color:'.$bg_color.'"' : ''); ?>>
    <div class="grid-container">
      <div class="grid-x align-center align-middle text-center">
        <div class="cell">
          <ul class="gallery-grid">
            <?php foreach($grid_images as $image): ?>
              <li <?= (!empty($image) ? 'style="background-image:url('.$image["url"].')"' : ''); ?>>
                <a href="<?= $image['url']; ?>"><img style="visibility:hidden" src="<?= $image['url']; ?>" alt="" /></a>
              </li>
            <?php endforeach; ?>
          </ul>
        </div>
      </div>
    </div>
</section>