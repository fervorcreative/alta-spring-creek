<?php

$tagline = get_sub_field('tagline');
$description = get_sub_field('description');
$bg_color = get_sub_field('bg_color');
$section_label = get_sub_field('layout_title'); ?>

<section class="intro section bg-light-gray tagline">
  <div class="grid-container">
    <div class="grid-x text-center">
      <div class="cell medium-offset-2 medium-8">
        <h1 class="color-primary" data-aos="fade-up"><?= $tagline; ?></h1>
        <div class="tagline-description" data-aos="fade-up"><?= (!empty($description) ? '<p>'.$description.'</p>' : ''); ?></div>
      </div>
    </div>
  </div>
</section>