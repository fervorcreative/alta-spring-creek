<?php
/**
 * Template part for displaying page content in page.php
 */

$id = get_the_ID();

if (have_rows( 'sections', $id )) :
  while ( have_rows( 'sections', $id ) ) : the_row();
    get_template_part( 'partials/' . get_row_layout() );
  endwhile;
endif; ?>

<!--<section class="section">
	<div class="grid-container">
		<header class="grid-x">
			<div class="cell">
				<?php the_title( '<h1 class="page__title">', '</h1>' ); ?>
			</div>
		</header>

		<div class="grid-x">
			<div class="cell">
				<?php the_content(); ?>
			</div>
		</div>
	</div>
</section>-->
