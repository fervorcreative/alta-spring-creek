<?php
/**
 * The front page template
 */

get_header(); ?>

  <?php if ( have_posts() ) : ?>

    <?php while ( have_posts() ) : the_post(); ?>
      <?php
      $hero = get_field( 'hero');
      $h_img = $hero['hero_image'];
      $h_title = $hero['hero_title'];
      $h_subtitle = $hero['hero_subtitle'];
      $h_btn = $hero['button'];
      $h_btn_link = $h_btn['button_link'];
      $h_btn_text = $h_btn['button_text'];
      ?>
      <section id="hero" class="section hero">
        <div class="grid-container full">
          <div class="grid-x">
            <div class="cell">
              <div class="hero-box">
                <div class="hero-content" data-aos="fade-up">
                  <h1 class="hero-title"><?php echo esc_html( $h_title ); ?></h1>
                  <h4 class="hero-subtitle"><?php echo esc_html( $h_subtitle ); ?></h4>
                  <a class="hero__link button" href="<?php echo esc_html( $h_btn_link ); ?>"><?php echo esc_html( $h_btn_text ); ?></a>
                </div>
                <div class="hero-video">
                  <video class="video video--hero" autoplay loop muted preload loop>
                      <source src="<?php echo get_template_directory_uri(); ?>/assets/media/alta-springcreek-video.mp4" type="video/mp4">
                      <!-- <source src="<?php echo get_template_directory_uri(); ?>/assets/media/alta-springcreek-video.webm" type="video/webm">
                      <source src="<?php echo get_template_directory_uri(); ?>/assets/media/alta-springcreek-video.ogv" type="video/ogg"> -->
                      
                      <img src="<?php echo $h_img['url']; ?>" alt="<?php echo $h_img['alt']; ?>" class="video-fallback">
                  </video>
                  <div class="video-alt">
                    <?php echo wp_get_attachment_image($h_img['ID'], 'full'); ?>
                  </div>
                </div>
              </div>
              
              <?php //echo wp_get_attachment_image($h_img['ID'], 'full', "", array('class'=>'hero-image') ); ?>
              
            </div>
          </div>
        </div>
      </section>
      
      <?php 
      $intro = get_field( 'intro' );
      $i_title = $intro['heading'];
      $i_text = $intro['supporting_text'];
      ?>
      <section id="tagline" class="section section--intro intro">
        <div class="grid-container">
          <div class="grid-x text-center">
            <div class="cell medium-offset-2 medium-8">

              <div class="decorative-small" data-aos="fade-down" data-aos-delay="500"></div>

              <div class="intro" data-aos="fade-up">
                
                <h3><?php echo esc_html( $i_title ); ?></h3>
                <svg class="icon dots"><use xlink:href="<?php echo get_template_directory_uri(); ?>/dist/sprite.svg#dot-graphics"></use></svg>
                <?php echo $i_text; ?>
                
              </div>
            </div>
          </div>
        </div>
      </section>
      
      <?php 
      $two_boxes = get_field( 'two_column_box' );
      $l_box = $two_boxes['left_box'];
      $r_box = $two_boxes['right_box'];
      
      $l_box_img = $l_box['left_box_image'];
      $r_box_img = $r_box['right_box_image'];
      
      $l_box_btn = $l_box['button'];
      $r_box_btn = $r_box['button'];
      ?>
      <section id="residences-amenities" class="section section--amenities two-column box" data-aos="fade-left" data-aos-delay="300">
        <div class="grid-container full">
          <div class="grid-x">
            <div class="box-image residences cell large-6" style="background-image: url(/wp-content/uploads/2020/02/bg-residences.png)">
              <div class="box-image-inner" style="background-image: url(<?php echo $l_box_img['sizes']['large']; ?>)">
                <div class="box-content">
                  <h3><?php echo $l_box['left_box_title']; ?></h3>
                  <?php echo $l_box['left_box_content']; ?>
                  <p><a href="<?php echo esc_url( $l_box_btn['button_link'] ); ?>" class="button"><?php echo $l_box_btn['button_text']; ?></a></p>
                </div> <!-- .box-content -->
                <div class="arrow"></div>
                <img class="box-image-hidden" src="<?php echo $l_box_img['sizes']['large']; ?>" <?php echo $l_box_img['alt']; ?> />
              </div> <!-- .box-image-inner -->
            </div> <!-- .box-image -->

            <div class="box-image amenities cell large-6" style="background-image: url(/wp-content/uploads/2020/02/bg-residences.png)">
              <div class="box-image-inner" style="background-image: url(<?php echo $r_box_img['sizes']['large']; ?>)">
                <div class="box-content">
                  <h3><?php echo $r_box['right_box_title']; ?></h3>
                  <?php echo $r_box['right_box_content']; ?>
                  <p><a href="<?php echo esc_url( $r_box_btn['button_link'] ); ?>" class="button"><?php echo $r_box_btn['button_text']; ?></a></p>
                </div> <!-- .box-content -->
                <div class="arrow"></div>
                <img class="box-image-hidden" src="<?php echo $r_box_img['sizes']['large']; ?>" alt="<?php echo $r_box_img['alt']; ?>" />
              </div> <!-- .box-image-inner -->
            </div> <!-- .box-image -->
          </div>
        </div>
      </section>
      <?php
      $d_section = get_field( 'decorative_section' );
      $dimg1 = $d_section['image_1'];
      $dimg2 = $d_section['image_2'];
      $dimg3 = $d_section['image_3'];
      $dbtn = $d_section['button'];
      ?>
      <section id="neighborhood" class="section section--neighborhood three-column box">
      <div class="decorative-medium" data-aos="fade-up" data-aos-delay="500"></div>
        <div class="grid-container expanded">
          <div class="grid-x align-top">
            <div class="cell three-column-left medium-4 large-4" data-aos="fade-down">
              <?php echo wp_get_attachment_image($dimg1['ID'], 'full'); ?>
              <?php echo wp_get_attachment_image($dimg2['ID'], 'full'); ?>
            </div> <!-- .box-image -->

            <div class="cell three-column-center medium-4 large-4" data-aos="fade-right">
              <?php echo wp_get_attachment_image($dimg3['ID'], 'full'); ?>
            </div> <!-- .box-image -->

            <div class="cell three-column-right medium-4 large-4" data-aos="fade-left">
              <h3><?php echo $d_section['ds_title']; ?></h3>
              <?php echo $d_section['ds_content']; ?>
              <p><a href="<?php echo esc_url( $dbtn['button_link'] ); ?>" class="button"><?php echo $dbtn['button_text']; ?></a></p>
            </div> <!-- .box-image -->
          </div> <!-- .grid-container -->
        </div>
      </section>

      <section id="contact" class="section contact-form">
        <div class="grid-container">
          <div class="grid-x text-center align-center">
            <div class="cell" data-aos="fade-right">
              <h3>Contact Us</h3>
              <div class="form">
                <?php echo do_shortcode('[gravityform id="1" title="false" description="false" ajax="true"]'); ?>
              </div> <!-- .form -->
            </div> <!-- .cell -->
          </div> <!-- .grid-x -->
        </div> <!-- .grid-container -->
      </section>

      <section id="gmap" class="section">
        <div class="grid-container full">
          <div class="grid-x">
            <div class="cell" data-aos="fade-right">
              <div class="mapHolder">
                <div id="gmap" class="home-map" width="100%" height="400"></div>
              </div> <!-- .map-holder -->
            </div> <!-- .cell -->
          </div> <!-- .grid-x -->
        </div> <!-- .grid-container -->
      </section>

    <?php endwhile; ?>

  <?php else : ?>

    <?php get_template_part( 'partials/content', 'none' ); ?>

  <?php endif; ?>

<?php get_footer(); ?>
