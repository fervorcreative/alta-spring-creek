<?php 

$title = get_sub_field('title');
$copy = get_sub_field('copy'); 
$image = get_sub_field('image'); ?>



<section class="box bg-secondary">
  <div class="grid-container full">
    <div class="grid-x">
      <div class="box__image cell large-6 large-order-2" data-aos="fade">
        <img src="<?= $image['url']; ?>" alt="City view from the rooftop">
      </div>
      <div class="box__text cell large-6" data-aos="fade-left">
        <h3 class="display-h2 heading heading--center color-white text-center"><?= $title; ?></h3>
        <?= $copy; ?>
        <!--<a class="button button__small" href="#">View Amenities</a>-->
      </div>
    </div>
  </div>
</section>