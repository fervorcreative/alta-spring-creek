<?php
/**
 * The template for displaying archive pages
 */

get_header(); ?>

<main id="main" role="main">

  <?php if ( have_posts() ) : ?>

    <section class="section archive">
      <div class="grid-container">
        <header class="grid-x">
          <div class="cell">
            <?php
              the_archive_title( '<h1>', '</h1>' );
              the_archive_description( '<div class="archive__description">', '</div>' );
            ?>
          </div>
        </header>

        <div class="grid-x small-up-2">
          <?php while ( have_posts() ) : the_post(); ?>

            <?php get_template_part( 'partials/content', get_post_format() ); ?>

          <?php endwhile; ?>

          <?php the_posts_navigation(); ?>
        </div>
      </div>
    </section>

  <?php else : ?>

    <?php get_template_part( 'partials/content', 'none' ); ?>

  <?php endif; ?>


</main>

<?php get_footer(); ?>
