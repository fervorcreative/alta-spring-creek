<?php

$title = get_sub_field('title');
$copy = get_sub_field('copy'); 
$image = get_sub_field('image'); ?>

<section class="bg-secondary">
  <div class="grid-container full">
    <div class="grid-x align-center">
      <div class="box__image cell large-6" data-aos="fade">
        <img src="<?= $image['url']; ?>" alt="Sophisticated kitchen">
      </div>
      <div class="box__text cell large-6" data-aos="fade-right">
        <h3 class="display-h2 heading heading--center color-white text-center"><?= $title; ?></h3>
        <?= $copy; ?>
        <!--<a class="button button__small" href="#">View Floor Plans</a>-->
      </div>
    </div>
  </div>
</section>