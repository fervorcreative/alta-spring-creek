<?php
/*
    Template Name: Schedule a Tour page
*/
get_header(); ?>

<?php get_template_part( 'template-parts/content', 'hero' ); ?>

<?php while( have_posts() ) : the_post(); ?>

    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        
        
        
        <section id="contact" class="section tour-form">
            <div class="grid-container">
                <div class="grid-x text-center align-center">
                    <div class="cell">
                        <h3>Fill Out the Form Below</h3>
                        <div class="form" data-aos="fade-right">
                            <div class="gform_wrapper">
                                <div id="form-messages"></div>
                                <form action="" class="gf_tour_form" method="post" enctype="multipart/form-data">
                                    <div class="gform_body">
                                        <ul class="gform_fields">
                                            <li class="gfield">
                                                <label class="gfield_label" for="ApptDate">Tour Date</label>
                                                <div class="ginput_container">
                                                    <input id="date-field" name="ApptDate" type="text" class="input date-field" value="" required placeholder="Tour Date">
                                                </div>
                                            </li>
                                            <li class="gfield">
                                                <label class="gfield_label" for="ApptTime">Tour Time</label>
                                                <div class="ginput_container">
                                                    <select id="timeslots" name="ApptTime" class="dropdown hidden"></select>
                                                    <div class="loader"><p>Choose a date</p></div>
                                                </div>
                                                
                                            </li>
                                            <li class="gfield">
                                                <label class="gfield_label" for="FirstName">First Name</label>
                                                <div class="ginput_container">
                                                    <input type="text" name="FirstName" class="text" value="" placeholder="First Name" required>
                                                </div>
                                            </li>
                                            <li class="gfield">
                                                <label class="gfield_label" for="LastName">Last Name</label>
                                                <div class="ginput_container">
                                                    <input type="text" name="LastName" class="text" value="" placeholder="Last Name" required>
                                                </div>
                                            </li>
                                            <li class="gfield">
                                                <label class="gfield_label" for="Email">Email Address</label>
                                                <div class="ginput_container">
                                                    <input type="email" name="Email" class="text" value="" placeholder="Email Address" required>
                                                </div>
                                            </li>
                                            <li class="gfield">
                                                <label class="gfield_label" for="Phone">Phone Number</label>
                                                <div class="ginput_container">
                                                    <input type="text" name="Phone" class="text" value="" placeholder="Phone Number" required>
                                                </div>
                                            </li>
                                            <li class="gfield">
                                                <label class="gfield_label" for="DesiredMoveinDate">Desired Move-in Date</label>
                                                <div class="ginput_container">
                                                    <input type="text" name="DesiredMoveinDate" id="movin-date" class="text movin-date" value="" placeholder="Desired Move-in Date">
                                                </div>
                                            </li>
                                            <li class="gfield">
                                                <?php
                                                    $bedrooms = '';
                                                    $bedrooms_array = array('studio', '1', '2', '3');
                                                    if( isset( $_GET['bedrooms']) && in_array( $_GET['bedrooms'], $bedrooms_array ) ) {
                                                        $bedrooms = $_GET['bedrooms'];
                                                    }
                                                ?>
                                                <label class="gfield_label <?php if( isset($_GET['bedrooms']) ) echo 'show-my-label'; ?>" for="DesiredBedrooms">Desired Bedrooms</label>
                                                <div class="ginput_container">
                                                    <input type="text" name="DesiredBedrooms" class="text" value="<?php echo $bedrooms; ?>" placeholder="Desired Bedrooms" <?php if( isset( $_GET['bedrooms']) ) echo 'disabled'; ?>>
                                                </div>
                                            </li>
                                            <li id="field_3_15" class="gfield gform_validation_container gfield_visibility_visible">
                                                <label class="gfield_label" for="input_3_15">Phone</label>
                                                <div class="ginput_container">
                                                    <input name="input_15" id="input_3_15" type="text" value="" autocomplete="off">
                                                </div>
                                                <div class="gfield_description" id="gfield_description__15">
                                                    This field is for validation purposes and should be left unchanged.
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="gform_footer">
                                        <?php
                                            $floorplan = '';
                                            if( isset( $_GET['floorplan']) ) {
                                                $floorplan = $_GET['floorplan'];
                                            }
                                        ?>
                                        <input type="hidden" name="FloorPlan" class="text" value="<?php echo $floorplan; ?>">
                                        <button id="tour-button" type="submit" value="Submit" name="Submit" class="gform_button button">
                                            <svg class="icon arrow"><use xlink:href="<?php echo get_template_directory_uri(); ?>/dist/sprite.svg#arrow-right"></use></svg> Submit
                                        </button>
                                        <!-- <input type="submit" value="Submit" name="Submit" class="gform_button button"> -->
                                    </div>
                                </form>
                            </div>
                        </div>
                        <?php //gravity_form( 3, false, false, false, null, true, 1, true ); ?>
                    </div> <!-- .cell -->
                </div> <!-- .grid-x -->
            </div> <!-- .grid-container -->
        </section>
        
        
    </article>
    
    
    
<?php endwhile; ?>
<?php get_footer(); ?>