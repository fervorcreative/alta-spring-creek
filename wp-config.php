<?php

  // Define Environment Variables
  $environment = new stdClass();
  $environment->local = '/wp-config-local.php';
  $environment->staging = '/wp-config-staging.php';
  $environment->testing = '/wp-config-testing.php';

  // Dynamically Set Environment Constants
  define( 'WP_ENV_LOCAL', file_exists( ABSPATH . $environment->local ) );
  define( 'WP_ENV_STAGING', file_exists( ABSPATH . $environment->staging ) );
  define( 'WP_ENV_TESTING', file_exists( ABSPATH . $environment->testing ) );

  if ( WP_ENV_LOCAL ) {
    require( ABSPATH . $environment->local );

  } elseif ( WP_ENV_STAGING ) {
    require( ABSPATH . $environment->staging );

  } elseif ( WP_ENV_TESTING ) {
    require( ABSPATH . $environment->testing );

  } else {
    define( 'WP_ENV_PRODUCTION', true );

    // Production Environment
    $mysql_hostname = 'localhost:3306';
    $mysql_username = '';
    $mysql_password = '';
    $mysql_database = 'admin_';

    // For Multisite
    // define( 'DOMAIN_CURRENT_SITE', 'altaspringcreek.local' );
    // define( 'SUNRISE', 'on' );

    define( 'ANALYTICS_PROFILE', '' );
  }

  // Configure MySQL Database Settings
  define( 'DB_USER', $mysql_username );
  define( 'DB_PASSWORD', $mysql_password );
  define( 'DB_NAME', $mysql_database );
  define( 'DB_HOST', $mysql_hostname );

  define( 'DB_CHARSET', 'utf8' );
  define( 'DB_COLLATE', '' );

  // For Multisite
  // define('MULTISITE', true);
  // define('SUBDOMAIN_INSTALL', false);
  // define('PATH_CURRENT_SITE', '/');
  // define('SITE_ID_CURRENT_SITE', 1);
  // define('BLOG_ID_CURRENT_SITE', 1);

	$table_prefix = 'wp3751';
	
	// WordPress Security Keys
	// https://api.wordpress.org/secret-key/1.1/salt/
	define('AUTH_KEY',         '7As{fc`FpslK$--+PL`gp6!%G6-XDJe_->s7RLXw%SDy4~]~x7;+9:;2DEs|s$ @');
  define('SECURE_AUTH_KEY',  'z?=:zg=)YWd^U-6k`IyiTLV-wwDZ+_31 Q8J=Ki]ZP@fa#/<f!yqD14_f_>Amxux');
  define('LOGGED_IN_KEY',    '$SDLZTZ=vPCG>CD>l4X&ooWy2Y**/X;:j}7PAW[E8xlM=o/QztDNU5s:z0>CN{-!');
  define('NONCE_KEY',        '/|v`ahcM6?`ZO25&Sm[5X|o:6YhGo|Izb@r9E0nJZy!?}KVZg8(-im#Y;ks}{hv+');
  define('AUTH_SALT',        'X 59k8o4#-o;2SP`.26#Y#kPzT{aCv8f/uX>C}Eo)q.~!m|05];R{Vw9Q|3dL/;$');
  define('SECURE_AUTH_SALT', ';TKMrAI:?S&|9sZ1T6ba-b~9E)z9$::^AudCYFe8}V->GmtHITa)GAI 2c|/B]D?');
  define('LOGGED_IN_SALT',   'CT87c71wc8Ue%yUFX^mH&K+hHp]+{$}(t)wTt_%Tzu?{+ g/)5GceH8HDg&%{]x%');
  define('NONCE_SALT',       'K>#[%j<c68>f]In (X3h1DdW>,Gc#-L(GOp|XndOMN$W^s>uhep&wxcv.yaV1fjQ');
	
	// Wordpress Application Options + Settings
	// http://codex.wordpress.org/Editing_wp-config.php
	define( 'WP_HOME', 'http://altaspringcreek.local' );
	define( 'WP_SITEURL', 'http://altaspringcreek.local' );
	
	// Disable Editing Theme/Plugin Files in Production Environment 
	//define('DISALLOW_FILE_EDIT', true); 
	//define('DISALLOW_FILE_MODS', true); 
	
	define( 'AUTOSAVE_INTERVAL', 300 );
	define( 'WP_POST_REVISIONS', false );
	
	// Set true if cache plugin installed
	define( 'WP_CACHE', false );
	define( 'WP_MEMORY_LIMIT', '128M' );
	
	if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
	}
	
	require_once( ABSPATH . 'wp-settings.php' );

?>