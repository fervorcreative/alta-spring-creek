<?php
/**
 * The front page template
 */

get_header(); ?>

  <?php if ( have_posts() ) : ?>

    <?php while ( have_posts() ) : the_post(); ?>

      <section id="hero" class="section hero">
        <div class="grid-container full">
          <div class="grid-x">
            <div class="hero-box cell" data-aos="fade">
              <div class="hero-content">
                <h2 class="hero-title" data-aos="fade-up">Comfort + Nature</h2>
                <h4 class="hero-subtitle" data-aos="fade-up" data-aos-delay="300">Premier Apartment Homes</h4>
                <a class="hero__link button" href="#contact">Join Our VIP List</a>
              </div>
              <img class="hero-image" src="<?php bloginfo('template_directory'); ?>/assets/images/home-hero.jpg" alt="Luxurious common room.">
            </div>
          </div>
        </div>
      </section>

      <section id="tagline" class="section intro">
        <div class="grid-container">
          <div class="grid-x text-center">
            <div class="cell medium-offset-2 medium-8">

              <div class="decorative-small" data-aos="fade-down" data-aos-delay="500"></div>

              <div class="alta-carousel" data-aos="fade-up">
                <div>
                  <h3>Grounded in nature. Connected to community.</h3>
                  <p>Set in a refreshing, natural landscape, Alta Spring Creek’s relaxed, modern living spaces deliver on the promise of a down-to-earth lifestyle without the long commute.</p>
                </div>
                <!--<div>
                  <h3>Modern meets natural</h3>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean justo libero, varius ac vestibulum sit amet, gravida nec magna. Aliquam non nunc non neque volutpat suscipit at eu nibh.</p>
                </div> 
                <div>
                  <h3>Modern meets natural</h3>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean justo libero, varius ac vestibulum sit amet, gravida nec magna. Aliquam non nunc non neque volutpat suscipit at eu nibh.</p>
                </div>-->
              </div>
            </div>
          </div>
        </div>
      </section>

      <section id="residences-amenities" class="section two-column box" data-aos="fade-left" data-aos-delay="300">
        <div class="grid-container full">
          <div class="grid-x">
            <div class="box-image residences cell large-6" style="background-image: url(/wp-content/uploads/2020/02/bg-residences.png)">
              <div class="box-image-inner" style="background-image: url(/wp-content/uploads/2020/02/residences-1.png)">
                <div class="box-content">
                  <h3>Residences</h3>
                  <p>Enjoy modern, natural living from top to bottom, with comfortable homes that feature 9-foot ceilings, wood style flooring, granite countertops, spacious walk-in closets and private yards.</p>
                </div> <!-- .box-content -->
                <div class="arrow"></div>
                <img class="box-image-hidden" src="/wp-content/uploads/2020/02/residences-1.png)" />
              </div> <!-- .box-image-inner -->
            </div> <!-- .box-image -->

            <div class="box-image amenities cell large-6" style="background-image: url(/wp-content/uploads/2020/02/bg-residences.png)">
              <div class="box-image-inner" style="background-image: url(/wp-content/uploads/2020/02/amenities.png)">
                <div class="box-content">
                  <h3>Amenities</h3>
                  <p>Alta Spring Creek brings together all the essentials for work and play, like an on-site dog park, resort-style pool, state-of-the-art fitness center, and a conference room for business meetings.</p>
                </div> <!-- .box-content -->
                <div class="arrow"></div>
                <img class="box-image-hidden" src="/wp-content/uploads/2020/02/amenities.png" />
              </div> <!-- .box-image-inner -->
            </div> <!-- .box-image -->
          </div>
        </div>
      </section>

      <section id="neighborhood" class="section three-column box">
      <div class="decorative-medium" data-aos="fade-up" data-aos-delay="500"></div>
        <div class="grid-container expanded">
          <div class="grid-x align-center">
            <div class="cell three-column-left medium-4 large-4" data-aos="fade-down">
              <img src="/wp-content/uploads/2020/02/neighborhood1.png" />
              <img src="/wp-content/uploads/2020/02/neighborhood2.png" />
            </div> <!-- .box-image -->

            <div class="cell three-column-center medium-4 large-4" data-aos="fade-right">
              <img src="/wp-content/uploads/2020/02/neighborhood3.png" />
            </div> <!-- .box-image -->

            <div class="cell three-column-right medium-4 large-4" data-aos="fade-left">
              <h3>Neighborhood</h3>
              <p>Live in your own natural oasis in the center of a thriving community. The Spring Creek Greenbelt trail system is right outside your door, and dozens of retailers, employers and restaurants are only minutes away.</p>
              <p>In fact, you’ll be less than five-minutes by car from CityLine, under 15 minutes from major business centers—like the 190, Telecom, and Platinum corridors—and just 30 minutes from downtown Dallas and both major airports.</p>
            </div> <!-- .box-image -->
          </div> <!-- .grid-container -->
        </div>
      </section>

      <section id="contact" class="section contact-form">
        <div class="grid-container">
          <div class="grid-x text-center align-center">
            <div class="cell" data-aos="fade-right">
              <h3>Schedule A Tour</h3>
              <div class="form">
                <?php echo do_shortcode('[gravityform id="1" title="false" description="false" ajax="true"]'); ?>
              </div> <!-- .form -->
            </div> <!-- .cell -->
          </div> <!-- .grid-x -->
        </div> <!-- .grid-container -->
      </section>

      <section id="gmap" class="section">
        <div class="grid-container full">
          <div class="grid-x">
            <div class="cell" data-aos="fade-right">
              <div class="mapHolder">
                <div id="locations-map" width="100%" height="400"></div>
              </div> <!-- .map-holder -->
            </div> <!-- .cell -->
          </div> <!-- .grid-x -->
        </div> <!-- .grid-container -->
      </section>

    <?php endwhile; ?>

  <?php else : ?>

    <?php get_template_part( 'partials/content', 'none' ); ?>

  <?php endif; ?>

<?php get_footer(); ?>
