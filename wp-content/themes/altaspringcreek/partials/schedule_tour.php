<?php

$bg_color = get_sub_field('bg_color');
$title = get_sub_field('title');
$copy = get_sub_field('copy');
$form_id = get_sub_field('form_id'); ?>

<section id="contact" class="section" <?= !empty($bg_color) ? 'style="background-color:#f7f4f1"' : ''; ?>>
  <div class="grid-container">
    <div class="grid-x">
      <div class="cell medium-10 medium-offset-1 large-5 large-offset-0" data-aos="fade-right">
        <h3 class="display-h2 heading"><?= $title; ?></h3>
        <p class="text-large"><?= $copy; ?></p>
      </div>
      <div class="cell medium-10 medium-offset-1 large-5 large-offset-2">
        <div class="form">
          <?php echo do_shortcode('[gravityform id="'.$form_id.'" title="false" description="false" ajax="true"]'); ?>
        </div>
      </div>
    </div>
  </div>
</section>

