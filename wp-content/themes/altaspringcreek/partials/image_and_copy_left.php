<?php 
$bg_color = get_sub_field( 'section_bg_color');
$title = get_sub_field('title');
$copy = get_sub_field('copy'); 
$image = get_sub_field('image'); 
$fullwidth = get_sub_field( 'full_width'); // boolean
$offset_copy = get_sub_field( 'offset_copy'); // boolean
$top_padding = get_sub_field( 'section_padding_top' );
$bottom_padding = get_sub_field( 'section_padding_bottom' ); ?>

<section class="section text-and-image <?php padding_top_classes(); ?> <?php padding_bottom_classes($bottom_padding); ?>" <?php if( $bg_color ) echo 'style="background-color: '. $bg_color.';"'; ?>>
  <div class="grid-container <?php echo ($fullwidth ? 'full' : ''); ?> ">
    <div class="grid-x align-center align-middle">
      <div class="box__text cell large-6 <?php echo ($offset_copy ? 'offset-text' : ''); ?>" data-aos="fade-right">
        <div class="box__text_inner">
          <h3 class="display-h2 heading"><?php echo $title; ?></h3>
          <svg class="icon dots"><use xlink:href="<?php echo get_template_directory_uri(); ?>/dist/sprite.svg#dot-graphics"></use></svg>
          <?php echo $copy; ?>
        </div>
      </div>
      <div class="box__image cell large-6" data-aos="fade-left">
        <?php echo wp_get_attachment_image( $image['ID'], 'gal-thumb' ); ?>
      </div>
      
    </div>
  </div>
</section>