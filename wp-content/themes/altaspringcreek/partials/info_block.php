<?php

$content = get_sub_field('content');

?>

<section class="section info-block section--reduce bg-secondary">
  <div class="grid-container">
    <div class="grid-x">
      <div class="cell medium-8 medium-offset-2 text-center color-white" data-aos="fade-up">
        <?= $content; ?>
      </div>
    </div>
  </div>
</section>