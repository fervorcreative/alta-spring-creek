<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no" />
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <?php wp_head(); ?>
  </head>
  <body <?php if ( get_body_class() ) body_class(); ?>>
    <?php
      // Dynamically add sprite.svg if it was created
      $sprite_path = get_template_directory() . '/dist/sprite.svg';

      if ( file_exists( $sprite_path ) ) { ?>
        <script type="text/javascript">
          //Grab SVG Sprite and AJAX in so it can be cached
          var ajax = new XMLHttpRequest();
          ajax.open("GET", "<?php bloginfo('template_directory'); ?>/dist/sprite.svg", true);
          ajax.onload = function(e) {
            var svg = $(ajax.responseText);
            document.body.insertBefore(svg.get(0), document.body.childNodes[0]);
          }
          ajax.send();
        </script>
      <?php }
    ?>

    <div class="site">
      <a href="#main" class="screen-reader-text">Skip to main content</a>
      <header class="header" role="banner">
        <div class="grid-container">
          <div class="grid-x grid-padding-x align-center">
            <div class="cell shrink">
              <a class="header__logo" href="<?php echo esc_url( home_url( '/' ) ); ?>">
                <img src="<?php bloginfo('template_directory'); ?>/assets/images/logo-alta-potrero.svg" alt="Alta Potrero">
              </a>
            </div>
          </div>
        </div>
      </header>
