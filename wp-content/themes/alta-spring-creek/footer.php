  </main>

  <footer id="footer" class="section footer bg-black color-white" role="contentinfo">
    <div class="grid-container footer__info">
      <div class="grid-x align-middle align-center text-center">
        <div class="cell small-12 medium-12 large-shrink footer-logo">
          <img src="/wp-content/uploads/2020/02/alta-footer-logo.png" alt="Alta Spring Creek Logo" />
        </div> <!-- .footer-logo -->
        <div class="cell large-auto copyright">
          <p class="footer__copyright">
            &copy; <?php echo date('Y'); ?> Alta Spring Creek
            <span>|</span>
            Professionally Managed by Wood Partners
            <span>|</span>
            <a href="https://www.woodpartners.com/privacy-policy/" target="_blank">Privacy Policy</a>
            <span>|</span>
            <a href="https://www.woodpartners.com/digital-accessibility/?_ga=2.172898019.1930056624.1579276808-1612636055.1579276808" target="_blank">Digital Accessibility</a>
          </p>
        </div>
        <div class="footer__icons cell large-shrink text-center">
          <a href="https://www.woodpartners.com/" target="_blank">
            <img class="footer__icon-p" src="/wp-content/uploads/2020/01/wood-partners-logo.png" alt="Wood Partners">
          </a>
          <a href="https://www.woodpartners.com/property-management" target="_blank">
            <img class="footer__icon-r" src="/wp-content/uploads/2020/01/wood-residential-logo.png" alt="Wood Partners Residential Services">
          </a>
          <a href="https://www.woodpartners.com/wp-content/uploads/2017/11/WRS-Pet-Policy.pdf" target="_blank">
            <img class="footer__icon-pets" src="/wp-content/uploads/2020/01/wood-pets-logo.png" alt="WRS Loves Pets">
          </a>
          <a href="https://www.hud.gov/program_offices/fair_housing_equal_opp" target="_blank">
            <img class="footer__icon-f" src="/wp-content/uploads/2020/01/equal-housing-logo.png" alt="Equal Housing and Accessibility">
          </a>
        </div>
      </div>
    </div>

    <div class="grid-container m_copyright">
      <div class="grid-x align-middle align-center text-center">
        <div class="cell large-auto copyright">
          <p class="footer__copyright">
            &copy; <?php echo date('Y'); ?> Alta Spring Creek
            <span>|</span>
            Professionally Managed by Wood Partners
            <span>|</span>
            <a href="https://www.woodpartners.com/privacy-policy/" target="_blank">Privacy Policy</a>
            <span>|</span>
            <a href="https://www.woodpartners.com/digital-accessibility/?_ga=2.172898019.1930056624.1579276808-1612636055.1579276808" target="_blank">Digital Accessibility</a>
          </p>
        </div> <!-- .copyright -->
      </div> <!-- .grid-x -->
    </div> <!-- .m_copyright -->
  </footer>
</div>

<?php wp_footer(); ?>

</body>
</html>
