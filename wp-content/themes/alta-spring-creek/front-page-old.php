<?php
/**
 * The front page template
 */

get_header(); ?>

<main id="main" role="main" class="front-page-landing">

  <?php if ( have_posts() ) : ?>

    <?php while ( have_posts() ) : the_post(); ?>

    <script>
// Create a clone of the menu, right next to original
$('.site-header').addClass('original').clone().insertAfter('.site-header').addClass('cloned').css('position','fixed').css('top','0').css('margin-top','0').css('z-index','500').removeClass('original').hide();

scrollIntervalID = setInterval(stickIt, 10);

function stickIt() {
 
 var orgElementPos = $('.original').offset();
  orgElementTop = orgElementPos.top;               

  if ($(window).scrollTop() >= (orgElementTop)  ) {
    // scrolled past the original position; now only show the cloned, sticky element.

    // Cloned element should always have same left position and width as original element.     
    orgElement = $('.original');
    coordsOrgElement = orgElement.offset();
    leftOrgElement = coordsOrgElement.left;  
    widthOrgElement = orgElement.css('width');
    $('.cloned').css('left',leftOrgElement+'px').css('top',0).css('width',widthOrgElement).show();
    $('.original').css('visibility','hidden');
  } else {
    // not scrolled past the menu; only show the original menu.
    $('.cloned').hide();
    $('.original').css('visibility','visible');
  }
}
</script>

      <section class="hero bg-black">
        <div class="grid-container full">
          <div class="grid-x">
            <div class="hero__box cell" data-aos="fade">
              <div class="hero__content">
                <h2 class="hero__title display-h1">Modern Living in San Francisco</h2>
                <a class="hero__link button email-me" href="#contact">Email me when tours begin</a>
                <a class="hero__link button view-gallery" href="/gallery">View Gallery</a>
              </div>
              <img class="hero__image" src="<?php bloginfo('template_directory'); ?>/assets/images/hero.jpg" alt="Luxurious common room.">
            </div>
          </div>
        </div>
      </section>

      <section class="intro section bg-light-gray">
        <div class="grid-container">
          <div class="grid-x text-center">
            <div class="cell medium-offset-2 medium-8">
              <h1 class="color-primary" data-aos="fade-up">Alta Potrero is reimagining city living in one of San Francisco’s most sought-after neighborhoods.</h1>
            </div>
          </div>
        </div>
      </section>

      <section class="box bg-secondary">
        <div class="grid-container full">
          <div class="grid-x">
            <div class="box__image cell large-6 large-order-2" data-aos="fade">
              <img src="<?php bloginfo('template_directory'); ?>/assets/images/amenities.jpg" alt="City view from the rooftop">
            </div>
            <div class="box__text cell large-6" data-aos="fade-left">
              <h3 class="display-h2 heading heading--center color-white text-center">Mindful Amenities</h3>
              <ul class="color-white">
                <li>Two 360 degree rooftop terraces with outdoor stage, bbq grills, overhead heaters, seating areas and fire pits</li>
                <li>Fitness center with Peloton bike</li>
                <li>Landscaped courtyard with two firepits, tree grove and view of Potrero Hill</li>
                <li>Resident storage, bike storage and 24-hour package pick-up</li>
                <li>Private conference / dining room</li>
                <li>Private lounge / co-working space</li>
                <li>Ground-level neighborhood retail</li>
                <li>Latch<sup>™</sup> keyless smartphone operated unit and building entries</li>
                <li>Dog wash</li>
                <li>Laundry lockers</li>
              </ul>
            </div>
          </div>
        </div>
      </section>

      <section class="bg-secondary">
        <div class="grid-container full">
          <div class="grid-x align-center">
            <div class="box__image cell large-6" data-aos="fade">
              <img src="<?php bloginfo('template_directory'); ?>/assets/images/residences.jpg" alt="Sophisticated kitchen">
            </div>
            <div class="box__text cell large-6" data-aos="fade-right">
              <h3 class="display-h2 heading heading--center color-white text-center">Modern, Inviting Residences</h3>
              <ul class="color-white">
                <li>Panelized Fischer & Paykel and Blomberg appliances</li>
                <li>Five burner gas cooktops and convection ovens</li>
                <li>Quartz countertops, backsplashes and islands</li>
                <li>Hard surface flooring throughout</li>
                <li>Climate controlled residences with air conditioning</li>
                <li>Bathrooms with backlit vanity mirrors, tile floors and soaking tubs. Master showers in select units</li>
                <li>Private balconies in select units</li>
                <li>Floor-to-ceiling windows with sweeping views</li>
                <li>Front-load washer and dryer</li>
              </ul>
            </div>
          </div>
        </div>
      </section>

      <section class="imagery bg-black">
        <div class="grid-container full">
          <div class="grid-x">
            <div class="cell" data-aos="fade">
              <img src="<?php bloginfo('template_directory'); ?>/assets/images/rendering.jpg" alt="Rendering of Alta Potrero.">
            </div>
          </div>
        </div>
      </section>

      <section id="contact" class="section">
        <div class="grid-container">
          <div class="grid-x">
            <div class="cell medium-10 medium-offset-1 large-5 large-offset-0" data-aos="fade-right">
              <h3 class="display-h2 heading">Keep in Touch</h3>
              <p class="text-large">Ready to make the move to Potrero Hill? Please submit your information to receive more information about living at Alta Potrero.</p>
            </div>
            <div class="cell medium-10 medium-offset-1 large-5 large-offset-2">
              <div class="form">
                <?php echo do_shortcode('[gravityform id="2" title="false" description="false" ajax="true"]'); ?>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section class="section section--reduce bg-secondary">
        <div class="grid-container">
          <div class="grid-x">
            <div class="cell medium-8 medium-offset-2 text-center color-white" data-aos="fade-up">
              <h3 class="display-h2 heading heading--center">For Retail Inquiries, Contact</h3>
              <p class="text-xlarge">
                <strong class="text-xxlarge">Mike Semmelmeyer</strong> <br>
                Vice President <br>
                Main Street Property Services, Inc. <br>
                License 01859585
              </p>
              <p class="text-xlarge">
                D: 925.444.3154 <br>
                C: 925.864.1662
              </p>
            </div>
          </div>
        </div>
      </section>

    <?php endwhile; ?>

  <?php else : ?>

    <?php get_template_part( 'partials/content', 'none' ); ?>

  <?php endif; ?>

</main>

<?php get_footer(); ?>
