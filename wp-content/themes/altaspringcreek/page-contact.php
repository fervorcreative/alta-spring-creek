<?php
/*
    Template Name: Contact page
*/
get_header(); ?>

<?php get_template_part( 'template-parts/content', 'hero' ); ?>

<?php while( have_posts() ) : the_post(); ?>

    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        
        <section class="section text-and-image paddingtop-small paddingbottom-large">
            <div class="grid-container full">
                <div class="grid-x align-center align-middle">
                    <div class="box__image cell large-6">
                        <div id="gmap" class="small-map">
                            ...loading map
                        </div>
                    </div>
                    <div class="box__text cell large-6 offset-text" data-aos="fade-left" data-aos-offset="300">
                        <div class="box__text_inner">
                            <svg class="icon dots"><use xlink:href="<?php echo get_template_directory_uri(); ?>/dist/sprite.svg#dot-graphics"></use></svg>
                            <p>Are you ready to enjoy everything that Alta Spring Creek has to offer? Contact us today by filling out the form below!</p>
                            
                            <?php 
                            $address = get_field( 'address', 'option' );
                            $city = get_field( 'city', 'option' );
                            $state = get_field( 'state', 'option' );
                            $zip = get_field( 'zip', 'option' ); 
                            
                            if( $address && $city && $state && $zip ) { ?> 
                            <h5 class="serif m-b-0">Address</h5>
                            <p><?php echo $address; ?><br>
                            <?php echo $city; ?>, <?php echo $state; ?> <?php echo $zip; ?></p>
                            <?php }
                            $hours = get_field( 'office_hours', 'option'); 
                            if( $hours ) { ?>
                            <h5 class="serif m-b-0">Office Hours</h5>
                            <p><?php echo $hours; ?></p>
                            <?php } ?>
                            <h5 class="serif m-b-0">Phone</h5>
                            <?php 
                            $phone_number = get_field( 'phone_number', 'option'); 
                            $call_number = str_replace( array('-', '.', '(', ')'), '', $phone_number );
                            ?>
                            <?php if( $phone_number ) {
                            echo '<p><a class="phone" href="tel:+1'. $call_number .'">'. $phone_number .'</a></p>';
                            } ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <section id="contact" class="section contact-form">
            <div class="grid-container">
                <div class="grid-x text-center align-center">
                    <div class="cell" data-aos="fade-right">
                    <h3>Contact Us</h3>
                    <div class="form">
                        <?php echo do_shortcode('[gravityform id=2 title=false description=false ajax=true]'); ?>
                    </div> <!-- .form -->
                    </div> <!-- .cell -->
                </div> <!-- .grid-x -->
            </div> <!-- .grid-container -->
        </section>
        
        
    </article>
    
    
    
<?php endwhile; ?>
<?php get_footer(); ?>