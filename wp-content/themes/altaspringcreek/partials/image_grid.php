<?php

$grid_images = get_sub_field('grid_images');
$bg_color = get_sub_field('background_color'); 
$top_padding = get_sub_field( 'section_padding_top' );
$bottom_padding = get_sub_field( 'section_padding_bottom' );?>

<section class="section image-grid <?php padding_top_classes(); ?> <?php padding_bottom_classes($bottom_padding); ?>" <?= (!empty($bg_color) ? 'style="background-color:'.$bg_color.'"' : ''); ?>>
    <div class="grid-container">
      <div class="grid-x align-center align-middle text-center gallery-grid">
            <?php $i=0; foreach($grid_images as $image): $i++;
              $video_link = get_field('video_link', $image['ID']);
              $aos_delay = $i * 100;
              if( $i%5 === 0 ) { ?>
              
                <div class="cell small-12" data-aos="fade">
                  <?php 
                  if($video_link) { ?>
                    <a href="<?php echo $video_link ?>" class="js-video">
                  <?php } else { ?>
                    <a href="<?php echo $image['sizes']['large']; ?>" class="js-gallery">
                  <?php } ?>
                    <?php echo wp_get_attachment_image( $image['ID'], 'gal-slim' ); ?>
                    <?php if($video_link) { ?>
                      <div class="play-icon">
                          <svg xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:cc="http://creativecommons.org/ns#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" id="play-icon" version="1.1" height="50" width="50" viewBox="0 0 1200 1200">
                            <path d="M 600,1200 C 268.65,1200 0,931.35 0,600 0,268.65 268.65,0 600,0 c 331.35,0 600,268.65 600,600 0,331.35 -268.65,600 -600,600 z M 450,300.45 450,899.55 900,600 450,300.45 z" id="path16995"></path>
                          </svg>
                      </div>
                    <?php } ?>
                  </a>
                </div>
                
              <?php } else { ?>

                <div class="cell small-6" data-aos="fade" data-aos-delay="<?php echo $aos_delay; ?>" data-image-id="<?php echo $image['ID']; ?>">
                  <?php
                   if($video_link) { ?>
                     <a href="<?php echo $video_link ?>" class="js-video">
                   <?php } else { ?>
                    <a href="<?php echo $image['sizes']['large']; ?>" class="js-gallery">
                  <?php } ?>
                    <?php echo wp_get_attachment_image( $image['ID'], 'gal-thumb' ); ?>
                    <?php if($video_link) { ?>
                      <div class="play-icon">
                          <svg xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:cc="http://creativecommons.org/ns#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" id="play-icon" version="1.1" height="50" width="50" viewBox="0 0 1200 1200">
                            <path d="M 600,1200 C 268.65,1200 0,931.35 0,600 0,268.65 268.65,0 600,0 c 331.35,0 600,268.65 600,600 0,331.35 -268.65,600 -600,600 z M 450,300.45 450,899.55 900,600 450,300.45 z" id="path16995"></path>
                          </svg>
                      </div>
                    <?php } ?>
                  </a>
                </div>
              <?php } ?>
              
            <?php endforeach; ?>
        </div>
      </div>
    </div>
</section>